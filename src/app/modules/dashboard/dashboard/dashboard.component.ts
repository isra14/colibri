import { ChangeDetectorRef, Component, OnDestroy, OnInit } from "@angular/core";
import { DashboardFacadeService } from "./dashboard-facade.service";
import * as pluginDataLabels from "chartjs-plugin-datalabels";
import { Subscription } from "rxjs";
import { MediaMatcher } from "@angular/cdk/layout";

export interface PeriodicElement {
  name: string;
  position: number;
  weight: number;
  symbol: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
  { position: 1, name: "Hydrogen", weight: 1.0079, symbol: "H" },
  { position: 2, name: "Helium", weight: 4.0026, symbol: "He" },
  { position: 3, name: "Lithium", weight: 6.941, symbol: "Li" },
  { position: 4, name: "Beryllium", weight: 9.0122, symbol: "Be" },
  { position: 5, name: "Boron", weight: 10.811, symbol: "B" },
  { position: 6, name: "Carbon", weight: 12.0107, symbol: "C" },
  { position: 7, name: "Nitrogen", weight: 14.0067, symbol: "N" },
  { position: 8, name: "Oxygen", weight: 15.9994, symbol: "O" },
  { position: 9, name: "Fluorine", weight: 18.9984, symbol: "F" },
  { position: 10, name: "Neon", weight: 20.1797, symbol: "Ne" },
];

@Component({
  selector: "app-dashboard",
  templateUrl: "./dashboard.component.html",
  styleUrls: ["./dashboard.component.scss"],
})
export class DashboardComponent implements OnInit, OnDestroy {

  public data: any[] = [];
  public mobileQuery: MediaQueryList;
  public displayedColumns: string[] = [];
  public columnsToDisplay: string[] = this.displayedColumns.slice();
  public suscripciones: Subscription = new Subscription();
  private _mobileQueryListener: () => void;


  constructor(public dashboardService: DashboardFacadeService, public changeDetectorRef: ChangeDetectorRef, public media: MediaMatcher,) {

    this.mobileQuery = media.matchMedia('(min-width: 1000px)');
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this._mobileQueryListener);
    this.recuperarModulo();

  }

  public animacionChart = "animate__animated animate__fadeInLeft";
  public clase = "col-md-12";
  public heightChart;

  ngOnInit(): void {
  }

  columsDisplay(item: []) {
    return item.slice();
  }

  ngOnDestroy() {
    this.dashboardService.reiniciarDash();
    this.suscripciones.unsubscribe();
    this.mobileQuery.removeListener(this._mobileQueryListener);

  }

  recuperarModulo() {
    let idModulo = JSON.parse(localStorage.getItem("idModulo"));

    this.dashboardService.mostrarDashboard({
      module: idModulo,
      "parametro": "{'key': 1,'value':'xxx}"
    });

    // if(idModulo === 1003){
    //   this.dashboardService.mostrarDashboard('purchase', '');
    // }
    // if(idModulo === 1002){
    //   this.dashboardService.mostrarDashboard('sales', '');
    // }

  }

  public barChartOptions = {
    scaleShowVerticalLines: false,
    responsive: true,
    plugins: {
      datalabels: {
        color: "white",
        anchor: "end",
        align: "end",
      },
    },
    legend: {
      display: true,
      labels: {
        fontColor: "white",
        fontSize: 10
      },
    },
    scales: {
      xAxes: [
        {
          id: "x-axis-0",
          position: "end",
          ticks: {
            fontColor: "rgb(255, 255, 255)",
            fontSize: 12

          },
        },
      ],
      yAxes: [
        {
          id: "y-axis-0",
          position: "left",

          ticks: {
            fontColor: "rgb(255, 255, 255)",
            fontSize: 12
          },
        },
        {
          id: "y-axis-1",
          position: "right",
          // gridLines: {
          //   color: 'white',
          // },
          ticks: {
            fontColor: "rgb(255, 255, 255)",
            fontSize: 12
          },
        },
      ],
    },
    annotation: {
      annotations: [
        {
          type: "line",
          mode: "vertical",
          scaleID: "x-axis-0",
          value: "March",
          borderColor: "white",
          borderWidth: 2,
          label: {
            enabled: true,
            fontColor: "white",
            content: "LineAnno",
            fontSize: 12
          },
        },
      ],
    },
  };

  public barChartPlugins = [pluginDataLabels];

  cargarDash() { }

  // addColumn() {
  //   const randomColumn = Math.floor(Math.random() * this.displayedColumns.length);
  //   this.columnsToDisplay.push(this.displayedColumns[randomColumn]);
  // }

  // removeColumn() {
  //   if (this.columnsToDisplay.length) {
  //     this.columnsToDisplay.pop();
  //   }
  // }

  // shuffle() {
  //   let currentIndex = this.columnsToDisplay.length;
  //   while (0 !== currentIndex) {
  //     let randomIndex = Math.floor(Math.random() * currentIndex);
  //     currentIndex -= 1;

  //     // Swap
  //     let temp = this.columnsToDisplay[currentIndex];
  //     this.columnsToDisplay[currentIndex] = this.columnsToDisplay[randomIndex];
  //     this.columnsToDisplay[randomIndex] = temp;
  //   }
  // }
}
