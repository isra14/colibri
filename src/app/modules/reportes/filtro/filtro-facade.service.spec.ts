import { TestBed } from '@angular/core/testing';

import { FiltroFacadeService } from './filtro-facade.service';

describe('FiltroFacadeService', () => {
  let service: FiltroFacadeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FiltroFacadeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
