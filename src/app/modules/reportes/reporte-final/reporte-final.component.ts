import { Component, OnInit } from '@angular/core';
import { PrintService } from 'src/app/services/utils/print.service';
import { environment } from 'src/environments/environment';
import { FiltroFacadeService } from '../filtro/filtro-facade.service';

@Component({
  selector: 'app-reporte-final',
  templateUrl: './reporte-final.component.html',
  styleUrls: ['./reporte-final.component.css']
})
export class ReporteFinalComponent implements OnInit {

  public fecha = new Date();
  public dataUsuario: any;
  public cargando:boolean = false;

  constructor(public filtroFacade:FiltroFacadeService) { 
    // this.dataUsuario = JSON.parse(localStorage.getItem(environment.dataUsuarioLocal));
  }

  ngOnInit(): void {
  }

  columsDisplay(item:[]){
  
    return item.slice();
  }

  

}
