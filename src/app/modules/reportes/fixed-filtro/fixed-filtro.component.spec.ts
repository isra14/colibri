import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FixedFiltroComponent } from './fixed-filtro.component';

describe('FixedFiltroComponent', () => {
  let component: FixedFiltroComponent;
  let fixture: ComponentFixture<FixedFiltroComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FixedFiltroComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FixedFiltroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
