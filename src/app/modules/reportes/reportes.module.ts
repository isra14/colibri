import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReportesRoutingModule } from './reportes-routing.module';
import { FiltroComponent } from './filtro/filtro.component';
import { ReporteFinalComponent } from './reporte-final/reporte-final.component';
import { FixedFiltroComponent } from './fixed-filtro/fixed-filtro.component';
import {MatCardModule} from '@angular/material/card';
import {MatSelectModule} from '@angular/material/select';
import { MatInputModule } from '@angular/material/input';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatButtonModule} from '@angular/material/button';
import { ReactiveFormsModule } from '@angular/forms';
import {MatDividerModule} from '@angular/material/divider';
import { MatNativeDateModule } from '@angular/material/core';
import { SharedModule } from 'src/app/shared/shared.module';
import { MatTableModule } from '@angular/material/table';
import { MatTabsModule } from '@angular/material/tabs';
import { MatIconModule } from '@angular/material/icon';
import {MatButtonToggleModule} from '@angular/material/button-toggle';
@NgModule({
  declarations: [FiltroComponent, ReporteFinalComponent, FixedFiltroComponent],
  imports: [
    CommonModule,
    ReportesRoutingModule,
    MatCardModule,
    MatSelectModule,
    MatInputModule,
    MatFormFieldModule,
    MatButtonModule,
    ReactiveFormsModule,
    MatDividerModule,
    MatNativeDateModule,
    SharedModule,
    MatTableModule,
    MatTabsModule,
    MatIconModule,
    MatButtonToggleModule
  ]
})
export class ReportesModule { }
