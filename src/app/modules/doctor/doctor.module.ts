import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DoctorRoutingModule } from './doctor-routing.module';
import { ListaPacientesComponent } from './lista-pacientes/lista-pacientes.component';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';

// MATERIAL
import { MatCardModule } from '@angular/material/card';
import {MatTabsModule} from '@angular/material/tabs';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatStepperModule } from '@angular/material/stepper';
import { MatRadioModule } from '@angular/material/radio';
import { MatBottomSheetModule } from '@angular/material/bottom-sheet';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatListModule } from '@angular/material/list';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatDialogModule } from '@angular/material/dialog';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatSelectModule } from '@angular/material/select';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatDatepickerModule} from '@angular/material/datepicker';
import { MatMomentDateModule } from "@angular/material-moment-adapter";
import { NgxMaskModule, IConfig, } from 'ngx-mask'
import {NgxPaginationModule} from 'ngx-pagination'; 


// import { NgxSpinnerModule } from 'ngx-spinner';


// PIPES
import { IdentidadPipe } from "../doctor/pipes/identidad.pipe";
import { NombresPipe } from "../doctor/pipes/nombres.pipe";
import { ApellidosPipe } from "../doctor/pipes/apellidos.pipe";
import { DireccionPipe } from "../doctor/pipes/direccion.pipe";
import { CorreoElectronicoPipe } from "../doctor/pipes/correo.pipe";
import { TelefonoMovilPipe  } from "../doctor/pipes/celular.pipe";
import { TelefonoFijoPipe } from "../doctor/pipes/fijo.pipe";
import { EdadPipe } from "../doctor/pipes/edad.pipe";
import { RolPipe } from "../doctor/pipes/rol.pipe";
import { UsuarioPipe } from "../doctor/pipes/usuario.pipe";




import { ListaUsuariosComponent } from './lista-usuarios/lista-usuarios.component';
import { ModalPacienteComponent } from './lista-pacientes/modal-paciente/modal-paciente.component';
import { ModalCitaComponent } from './lista-pacientes/modal-cita/modal-cita.component';
import { ModalAtendidoComponent } from './modalAtendido/modal-atendido/modal-atendido.component';


@NgModule({
  declarations: [ListaPacientesComponent,IdentidadPipe,NombresPipe,ApellidosPipe,DireccionPipe,CorreoElectronicoPipe,TelefonoMovilPipe,TelefonoFijoPipe,EdadPipe, ListaUsuariosComponent, 
    ModalPacienteComponent,RolPipe,UsuarioPipe, ModalCitaComponent, ModalAtendidoComponent],
  imports: [
    CommonModule,
    DoctorRoutingModule,
    MatCardModule,
    MatTabsModule,
    CommonModule,
    MatTabsModule,
    MatSlideToggleModule,
    MatToolbarModule,
    MatExpansionModule,
    MatBottomSheetModule,
    MatRadioModule,
    MatCheckboxModule,
    MatListModule,
    MatStepperModule,
    MatSelectModule,
    MatFormFieldModule,
    MatDialogModule,
    MatInputModule,
    MatTooltipModule,
    MatPaginatorModule,
    MatCardModule,
    MatDialogModule,
    MatButtonModule,
    MatIconModule,
    MatAutocompleteModule,
    // NgxSpinnerModule
    MatProgressSpinnerModule,
    FormsModule,
    ReactiveFormsModule,
    MatDatepickerModule,
    MatMomentDateModule,
    NgxMaskModule,
    NgxPaginationModule
  ]
})
export class DoctorModule { }
