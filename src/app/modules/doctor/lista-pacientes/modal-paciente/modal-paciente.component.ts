import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogContent, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
import { PacientesService } from 'src/app/services/pacientes/pacientes.service';

@Component({
  selector: 'app-modal-paciente',
  templateUrl: './modal-paciente.component.html',
  styleUrls: ['./modal-paciente.component.css']
})
export class ModalPacienteComponent implements OnInit {

  constructor(  @Inject(MAT_DIALOG_DATA) public data: any, public srvPaciente:PacientesService,private modal:MatDialog,private toastr:ToastrService) { }
  datosPersonalesForm:FormGroup
  etnias:any
  ngOnInit(): void {
  this.etnias=this.data.etnias
    this.srvPaciente.getPaciente(this.data.item.id,data=>{
        console.log(data);
        this.datosPersonalesForm.patchValue({
          id:data.data.table0[0].id,
          nombres:data.data.table0[0].nombres,
          identidad:data.data.table0[0].identidad,
          apellidos:data.data.table0[0].apellidos,
          fechaNacimiento:data.data.table0[0].fechaNacimiento,
          idSexo:data.data.table0[0].idSexo,
          idGrupoEtnico:data.data.table0[0].idGrupoEtnico,
          idGenero:data.data.table0[0].idGenero,
          direccion:data.data.table0[0].direccion,
          idNivelEducativo: data.data.table0[0].idNivelEducativo,
          ocupacion: data.data.table0[0].ocupacion,
          profesion: data.data.table0[0].profesion,
          idNacionalidad: data.data.table0[0].idNacionalidad,
          idMunicipio: data.data.table0[0].idMunicipio,
          correoElectronico: data.data.table0[0].correoElectronico,
          telefonoFijo: data.data.table0[0].telefonoFijo,
          telefonoMovil: data.data.table0[0].telefonoMovil,
          idDocumentoIdentificacion: data.data.table0[0].idDocumentoIdentificacion,
          colonia: data.data.table0[0].colonia,
          contactoEmergencia: data.data.table0[0].contactoEmergencia,
          cantidadPersonas: data.data.table0[0].cantidadPersonas,
          telefonoContactoEmergencia: data.data.table0[0].telefonoContactoEmergencia,
          aldea: data.data.table0[0].aldea,
          edificio: data.data.table0[0].edificio,
          cantidadHabitaciones: data.data.table0[0].cantidadHabitaciones,
          idTipoVivienda: data.data.table0[0].idTipoVivienda,
          direccionContacto: data.data.table0[0].direccionContacto,
          detalleCategoriaDireccion: data.data.table0[0].detalleCategoriaDireccion,
          idCategoriaDireccion: data.data.table0[0].idCategoriaDireccion,
          activoEvaluar: data.data.table0[0].activoEvaluar,
          idEstadoSegumiento: data.data.table0[0].idEstadoSegumiento,
          idCategoriaProfesion: data.data.table0[0].idCategoriaProfesion,
          idMotivoRegistro: data.data.table0[0].idMotivoRegistro,
          numHijos: data.data.table0[0].numHijos,
          idEstadoCivil: data.data.table0[0].idEstadoCivil,
          idReligion: data.data.table0[0].idReligion,
        })
    })
this.datosPersonalesForm= new FormGroup({
  id: new FormControl(''),
     nombres: new FormControl('',Validators.required),
     apellidos: new FormControl('',Validators.required),
     identidad: new FormControl('',Validators.required),
     fechaNacimiento: new FormControl(''),
     numHijos: new FormControl(''),
     idSexo: new FormControl(''),
     idEstadoCivil: new FormControl(''),
     idReligion: new FormControl(''),
     idNivelEducativo: new FormControl(''),
     ocupacion: new FormControl(''),
     profesion: new FormControl(''),
     idNacionalidad: new FormControl(''),
     idMunicipio: new FormControl(''),
     correoElectronico: new FormControl(''),
     telefonoFijo: new FormControl(''),
     telefonoMovil: new FormControl(''),
     idDocumentoIdentificacion: new FormControl(''),
     direccion: new FormControl(''),
     colonia: new FormControl(''),
     contactoEmergencia: new FormControl(''),
     cantidadPersonas: new FormControl(''),
     telefonoContactoEmergencia: new FormControl(''),
     aldea: new FormControl(''),
     edificio: new FormControl(''),
     cantidadHabitaciones: new FormControl(''),
     idTipoVivienda: new FormControl(''),
     direccionContacto: new FormControl(''),
     detalleCategoriaDireccion: new FormControl(''),
     idCategoriaDireccion: new FormControl(''),
     activoEvaluar: new FormControl(''),
     idEstadoSegumiento: new FormControl(''),
     idCategoriaProfesion: new FormControl(''),
     idGenero: new FormControl(''),
     idMotivoRegistro: new FormControl(''),
     idGrupoEtnico: new FormControl('')
})
  }

  sexo=[
    {
      id:1,
      nombreSexo:'MASCULINO'
    },
    {
      id:2,
      nombreSexo:'FEMENINO'
    }
  ]

  
  trabajo=[
    {
      id:1,
      nombreTrabajo:'SI'
    },
    {
      id:2,
      nombreTrabajo:'NO'
    }
  ]
  generos=[
    {
      idGenero:1,
      nombreGenero:'HETERESEXUAL'
    },
    {
      idGenero:2,
      nombreGenero:'HOMOSEXUAL'
    },
    {
      idGenero:3,
      nombreGenero:'BISEXUAL'
    },
    {
      idGenero:4,
      nombreGenero:'LESBIANA'
    }
  ]
  updatePerfil(){
    
    if (this.datosPersonalesForm.valid) {
      this.modal.closeAll()
      this.srvPaciente.updatePerfil(this.datosPersonalesForm.value,data=>{
        console.log(data)
        this.toastr.info('Realizado correctamente');
      })
    } else {
      this.toastr.warning('Llene los campos obligatorios');
      
    }

      
  }

  validarNumeros(event) {
    if ((event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105) && event.keyCode !== 190 && event.keyCode !== 110 && event.keyCode !== 8 && event.keyCode !== 9) {
        return false;
    } else {
        return true;
    }
  }

  validarLetras(event) {
    if(event.keyCode == 13) {
        event.preventDefault();
      }
    if ((event.keyCode < 48 || event.keyCode > 57) && event.keyCode!=3 && (event.keyCode < 96 || event.keyCode > 105) && event.keyCode !== 190 && event.keyCode !== 110 && event.keyCode !== 188 && event.keyCode !== 111 && event.keyCode !== 106 && event.keyCode !== 109 && event.keyCode !== 221 && event.keyCode !== 219 && event.keyCode !== 222 && event.keyCode !== 191 && event.keyCode !== 220 && event.keyCode !== 107 && event.keyCode !== 189 && event.keyCode !== 187 && event.keyCode !== 186) {
        return true;
    } else {
        return false;
    }
  }


}
