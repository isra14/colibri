import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { PacientesService } from 'src/app/services/pacientes/pacientes.service';
import { map } from 'rxjs/operators';
import { PageEvent } from '@angular/material/paginator';
import { FormControl } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { EncuestaAll } from '../interfaces/encuestas.interface';
import { ModalPacienteComponent } from './modal-paciente/modal-paciente.component';
import { ModalCitaComponent } from './modal-cita/modal-cita.component';
// import { EncuestaAll } from "../interfaces/encuestas.interface";
import moment from 'moment';
import { ModalAtendidoComponent } from '../modalAtendido/modal-atendido/modal-atendido.component';



@Component({
  selector: 'app-lista-pacientes',
  templateUrl: './lista-pacientes.component.html',
  styleUrls: ['./lista-pacientes.component.css']
})
export class ListaPacientesComponent implements OnInit {
  public suscription: Subscription = new Subscription();
  constructor(private modal:MatDialog, public srvPaciente:PacientesService) { }

pageActual0: number = 1;
pageActual1: number = 1;
pageActualEncuesta:number=1
// pageActual1: number = 1;
 pageActualDos: number = 1;
// pageActual3: number = 1;
// pageActual4: number = 1;
// pageActual5: number = 1;
// pageActual6: number = 1;
// pageActual7: number = 1;
// pageActual8: number = 1;
// pageActual9: number = 1;
// pageActual10: number = 1;
// pageActual11: number = 1;
// pageActual12: number = 1;
// pageActuall:number=1
resetFiltro()
{

  this.identidad='';
  this.nombres='';
  this.apellidos='';
  this.direccion='';
  this.correoElectronico='';
  this.telefonoMovil='';
  this.telefonoFijo='';
  this.edad='';
  this.pageActual0=0;
  this.pageActual1=0;

}

  public pacientesAll:any=[]
  public encuestas:any=[]
  public encuestasDetalles:any=[]
  public pacientesRegistradosMenMenor:any=[]
  public pacientesRegistradosMenMayor:any=[]
  public pacientesRegistradosWomenMenor:any=[]
  public pacientesRegistradosWomenMayor:any=[]
  public pacientesAnonimosMenMenor:any=[]
  public pacientesAnonimosMenMayor:any=[]
  public pacientesAnonimosWomenMenor:any=[]
  public pacientesAnonimosWomenMayor:any=[]

  // FILTROS
   public identidad='';
   public nombres='';
   public apellidos='';
   public direccion='';
   public correoElectronico='';
   public telefonoMovil='';
   public telefonoFijo='';
   public edad='';

  public pacientesAnonimosMen:any=[]
  public pacientesAnonimosWomen:any=[]
  public filtro = new FormControl('');
  ngOnInit(): void {
    this.getPacientes()
    this.getEtnias()
  }
  idPaciente:number
  tmpEncuenta:any
  getEncuesta(item,tmp){
    this.idPaciente=item.id;
    this.tmpEncuenta=tmp;
    const dialogRef = this.modal.open(tmp, {
      width: '900px',
  });
    this.srvPaciente.getEncuesta(item.id,data=>{
        this.encuestas=data.data.table0

    })
  }

  etnias:any
  getEtnias(){
    this.srvPaciente.getEtnias('',data=>{
      this.etnias=data.data.table0
    })
  }
  getPaciente(item){
    var obj={
      item,
      etnias:this.etnias
    }
    const dialogRef = this.modal.open(ModalPacienteComponent, {
      data:obj,
      width: '900px' 
  })
  dialogRef.afterClosed().subscribe(result=>{

    this.getPacientes()
  });
  }

  agendarCita(item){
    var obj={
      item
    }
    const dialogRef = this.modal.open(ModalCitaComponent, {
      data:obj,
      width: '1200px' 
  })
  dialogRef.afterClosed().subscribe(result=>{
    this.getPacientes()
  });
  }

  citaNow(itemCita){
    let newDate: moment.Moment = moment(new Date()).local();
    var now = moment().locale('es-HN').format('HH:mm:ss');
    var fecha = newDate.format("YYYY-MM-DD");
    var obj ={
    idPaciente:itemCita.id,
    idCita:0,
    idDoctor:'',
    fechaCita:fecha,
    horaCita:now,
    idEstadoCita:1,
    observaciones:'',
    dictamen:'',
    idDictamen:'',
    valoracionTelemedica:'',
    concluciones:'',
    resultadosExamenesPrevios:'',
}
            
    this.srvPaciente.saveCita(obj,data=>{
    var item=data.data.table0[0]
     var obj={
       item,
       idPaciente:itemCita.id
     }
     const dialogRef = this.modal.open(ModalAtendidoComponent, {
       data:obj,
       width: '1000px' 
     })

    })

  }
  verEncuestaDetalle(item,tmp){
    this.modal.closeAll();
    var obj={
       id:this.idPaciente,
       idDetalle:item.id
    }
         const dialogRef = this.modal.open(tmp, {
        width: '900px',
    });

    this.srvPaciente.getEncuestaDetalle(obj,(data:EncuestaAll)=>{
      this.encuestasDetalles=data.data.table0


  })
  }

  cerrarModalDetalles(){
    var item={
      id:this.idPaciente
    }

    this.getEncuesta(item,this.tmpEncuenta)

  }



  getPacientes(){
    this.srvPaciente.getPacientes(0,data=>{
      this.pacientesAll=data.data.table0
      for (let index = 0; index < this.pacientesAll.length; index++) {
        
         this.pacientesAll[index].edad=this.pacientesAll[index].edad.toString();
         this.pacientesAll[index].telefonoMovil=this.pacientesAll[index].telefonoMovil.replace('-','');
      }
      console.log(this.pacientesAll);
      // REGISTRADOS
      this.pacientesRegistradosMenMenor = this.pacientesAll.filter(value=> (value.perfilAnonimo == false &&value.idSexo == 1 && value.edad<=18))
      this.pacientesRegistradosMenMayor = this.pacientesAll.filter(value=> (value.perfilAnonimo == false &&value.idSexo == 1 && value.edad>18))
      this.pacientesRegistradosWomenMenor = this.pacientesAll.filter(value=> (value.perfilAnonimo == false &&value.idSexo == 2 && value.edad<=18))
      this.pacientesRegistradosWomenMayor = this.pacientesAll.filter(value=> (value.perfilAnonimo == false &&value.idSexo == 2 && value.edad>18))

      // ANONIMOS
      this.pacientesAnonimosMenMenor = this.pacientesAll.filter(value=> (value.perfilAnonimo == true &&value.idSexo == 1 && value.edad<=18))
      this.pacientesAnonimosMenMayor = this.pacientesAll.filter(value=> (value.perfilAnonimo == true &&value.idSexo == 1 && value.edad>18))
      this.pacientesAnonimosWomenMenor = this.pacientesAll.filter(value=> (value.perfilAnonimo == true &&value.idSexo == 2 && value.edad<=18))
      this.pacientesAnonimosWomenMayor = this.pacientesAll.filter(value=> (value.perfilAnonimo == true &&value.idSexo == 2 && value.edad>18))

      // this.spinnerServi.hide();

        

    })
  }

  
}
