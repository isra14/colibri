import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { DateAdapter } from '@angular/material/core';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import moment from 'moment';
import { ToastrService } from 'ngx-toastr';
import { PacientesService } from 'src/app/services/pacientes/pacientes.service';
import { UsuariosService } from 'src/app/services/usuarios/usuarios.service';
import { ModalAtendidoComponent } from '../../modalAtendido/modal-atendido/modal-atendido.component';

@Component({
  selector: 'app-modal-cita',
  templateUrl: './modal-cita.component.html',
  styleUrls: ['./modal-cita.component.css']
})
export class ModalCitaComponent implements OnInit {

  constructor( @Inject(MAT_DIALOG_DATA) public data: any, public srvPaciente: PacientesService, private srvUsers:UsuariosService, private modal:MatDialog, private toast:ToastrService,private _adapter: DateAdapter < any >) { }
citaForm:FormGroup
min:any
pageActual:number=1
  ngOnInit(): void {
this.min= new Date()
    this._adapter.setLocale('es-HN');
    this.initCitaForm();
    this.getDoctores();
    this.getCitas();
  }

  initCitaForm(){
    this.citaForm = new FormGroup({
      idPaciente: new FormControl(this.data.item.id),
      idCita: new FormControl(0),
      idDoctor: new FormControl('',Validators.required),
      fechaCita: new FormControl('',Validators.required),
      horaCita: new FormControl('',Validators.required),
      idEstadoCita:new FormControl(1),
      observaciones: new FormControl(''),
      dictamen: new FormControl(''),
      idDictamen: new FormControl(''),
      valoracionTelemedica: new FormControl(''),
      concluciones: new FormControl(''),
      resultadosExamenesPrevios: new FormControl(''),
    })
  }
  doctores:any=[]
  citas:any=[]
  loadingCitas:boolean
  getDoctores(){
    this.srvUsers.getUsuarios('',data=>{
      console.log(data,'DOCTORES')
      this.doctores=data.data.table0;
    })
  }
  getCitas(){
this.loadingCitas=true;
    this.srvPaciente.getCitasPaciente(this.data.item.id,data=>{
        this.citas=data.data.table0
        this.loadingCitas=false;

    })
  }
  saveCita(){
    let newDate: moment.Moment = moment(this.citaForm.value.fechaCita).local();
    this.citaForm.value.fechaCita = newDate.format("YYYY-MM-DD");
    if (this.citaForm.valid) {
      if (this.citaForm.value.idCita==0) {
        
        this.srvPaciente.saveCita(this.citaForm.value,data=>{
          this.toast.info('Realizado correctamente');
          this.citaForm.reset();
          this.citaForm.patchValue({idCita:0,idPaciente:this.data.item.id})
        })  
        
      } else {
        this.srvPaciente.updateCita(this.citaForm.value,data=>{
          this.toast.info('Realizado correctamente');
          this.citaForm.reset();
          this.citaForm.patchValue({idCita:0,idPaciente:this.data.item.id})
        })  
      }
      this.getCitas();
      }
    else
    {
      this.toast.warning('Llene los campos obligatorios')

    }
  }

  openAtendido(item){

    this.modal.closeAll();
    var obj={
      item,
      idPaciente:this.data.item.id
    }
    const dialogRef = this.modal.open(ModalAtendidoComponent, {
      data:obj,
      width: '1000px' 
    })
  }
  openNoAtendido(item) {
      var obj ={
        idPaciente:this.data.item.id,
        idCita: item.idCita,
        idDoctor: item.idDoctor,
        idEstadoCita: 3,
        fechaCita: item.fechaCita,
        horaCita: item.horaCita,
        observaciones: item.observaciones,
        dictamen: item.dictamen,
        idDictamen: item.idDictamen,
        valoracionTelemedica: item.valoracionTelemedica,
        concluciones: item.concluciones,
        resultadosExamenesPrevios: item.resultadosExamenesPrevios,
      }
   this.srvPaciente.updateCita(obj,data=>{
     this.citaForm.reset();
     this.citaForm.patchValue({idCita:0,idPaciente:this.data.item.id})
       if (data.hasError==true) {
         this.toast.error(data.errors[0].descripcion);
       }else{
       this.toast.success('Cambio de estado exitoso');
     }
     this.getCitas();
   }
   );
  
  }

  preUpdateCita(item){
    this.citaForm.patchValue({
      idPaciente:this.data.item.id,
      idCita: item.idCita,
      idDoctor: item.idDoctor,
      idEstadoCita: item.idEstadoCita,
      fechaCita: item.fechaCita,
      horaCita: item.horaCita,
      observaciones: item.observaciones,
      dictamen: item.dictamen,
      idDictamen: item.idDictamen,
      valoracionTelemedica: item.valoracionTelemedica,
      concluciones: item.concluciones,
      resultadosExamenesPrevios: item.resultadosExamenesPrevios,
    })

  }
  validarNumeros(event) {
    if ((event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105) && event.keyCode !== 190 && event.keyCode !== 110 && event.keyCode !== 8 && event.keyCode !== 9) {
        return false;
    } else {
        return true;
    }
  }

  validarLetras(event) {
    if(event.keyCode == 13) {
        event.preventDefault();
      }
    if ((event.keyCode < 48 || event.keyCode > 57) && event.keyCode!=3 && (event.keyCode < 96 || event.keyCode > 105) && event.keyCode !== 190 && event.keyCode !== 110 && event.keyCode !== 188 && event.keyCode !== 111 && event.keyCode !== 106 && event.keyCode !== 109 && event.keyCode !== 221 && event.keyCode !== 219 && event.keyCode !== 222 && event.keyCode !== 191 && event.keyCode !== 220 && event.keyCode !== 107 && event.keyCode !== 189 && event.keyCode !== 187 && event.keyCode !== 186) {
        return true;
    } else {
        return false;
    }
  }
}
