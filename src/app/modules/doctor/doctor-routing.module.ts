import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RolGuard } from 'src/app/guards/rol.guard';
import { ListaPacientesComponent } from '../doctor/lista-pacientes/lista-pacientes.component';
import { ListaUsuariosComponent } from './lista-usuarios/lista-usuarios.component';

const routes: Routes = [
  {
    path: 'listaPacientes',
    component: ListaPacientesComponent
  },
  {
    path: 'listaUsuarios',
    component: ListaUsuariosComponent,
    canActivate:[RolGuard]
  }
  ]
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class DoctorRoutingModule { }
