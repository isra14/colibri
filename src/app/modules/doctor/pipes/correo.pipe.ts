import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'correoElectronico'
})
export class CorreoElectronicoPipe implements PipeTransform {
   
    transform(value: any,arg:any): any {
     const resultPost=[]
     for(const post of value){
         if (post.correoElectronico==null) {
          post.correoElectronico='';
         }
         if(post.correoElectronico.toLowerCase().indexOf(arg.toLowerCase())>-1){
            resultPost.push(post);
         };
      
       };
       return resultPost;
     
  }
   
  }
