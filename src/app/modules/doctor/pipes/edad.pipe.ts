import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'edad'
})
export class EdadPipe implements PipeTransform {
   
    transform(value: any,arg:any): any {
     const resultPost=[]
     value.toString();
     for(const post of value){
         if (post.edad==null) {
          post.edad='';
         }
         if(post.edad.toLowerCase().indexOf(arg.toLowerCase())>-1){
            resultPost.push(post);
         };
      
       };
       return resultPost;
     
  }
   
  }
