import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'telefonoMovil'
})
export class TelefonoMovilPipe implements PipeTransform {
   
    transform(value: any,arg:any): any {
     const resultPost=[]
     for(const post of value){
         if (post.telefonoMovil==null) {
          post.telefonoMovil='';
         }
         
         if(post.telefonoMovil.toLowerCase().indexOf(arg.toLowerCase())>-1){
            resultPost.push(post);
         };
      
       };
       return resultPost;
     
  }
   
  }
