import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'direccion'
})
export class DireccionPipe implements PipeTransform {
   
    transform(value: any,arg:any): any {
     const resultPost=[]
     for(const post of value){
         if (post.direccion==null) {
          post.direccion='';
         }
         if(post.direccion.toLowerCase().indexOf(arg.toLowerCase())>-1){
            resultPost.push(post);
         };
      
       };
       return resultPost;
     
  }
   
  }
