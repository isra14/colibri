import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'telefonoFijo'
})
export class TelefonoFijoPipe implements PipeTransform {
   
    transform(value: any,arg:any): any {
     const resultPost=[]
     for(const post of value){
         if (post.telefonoFijo==null) {
          post.telefonoFijo='';
         }
         if(post.telefonoFijo.toLowerCase().indexOf(arg.toLowerCase())>-1){
            resultPost.push(post);
         };
      
       };
       return resultPost;
     
  }
   
  }
