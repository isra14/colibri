import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'apellidos'
})
export class ApellidosPipe implements PipeTransform {
   
    transform(value: any,arg:any): any {
     const resultPost=[]
     for(const post of value){
         if (post.apellidos==null) {
          post.apellidos='';
         }
         if(post.apellidos.toLowerCase().indexOf(arg.toLowerCase())>-1){
            resultPost.push(post);
         };
      
       };
       return resultPost;
     
  }
   
  }
