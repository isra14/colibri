import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'usuario'
})
export class UsuarioPipe implements PipeTransform {
   
    transform(value: any,arg:any): any {
     const resultPost=[]
     for(const post of value){
         if (post.usuario==null) {
          post.usuario='';
         }
         if(post.usuario.toLowerCase().indexOf(arg.toLowerCase())>-1){
            resultPost.push(post);
         };
      
       };
       return resultPost;
     
  }
   
  }
