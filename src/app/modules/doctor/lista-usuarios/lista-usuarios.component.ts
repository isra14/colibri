import { Component, OnInit } from '@angular/core';
import { PageEvent } from '@angular/material/paginator';
import { UsuariosService } from 'src/app/services/usuarios/usuarios.service';
import Swal from 'sweetalert2';


@Component({
  selector: 'app-lista-usuarios',
  templateUrl: './lista-usuarios.component.html',
  styleUrls: ['./lista-usuarios.component.css']
})
export class ListaUsuariosComponent implements OnInit {

  constructor(public srvUsers:UsuariosService) { }
public usuarios:any=[]
pageActual: number = 1;

 // FILTROS
 public identidad='';
 public nombres='';
 public apellidos='';
 public usuario='';
 public rol='';
  ngOnInit(): void {
    this.getUsers();
  }

  getUsers(){
    this.srvUsers.getUsuarios('',data=>{
    console.log(data)
    this.usuarios=data.data.table0
    })
  }


  
    //Paginacion
public pageSize = 10;
public page = 0;
public pageEvent!: PageEvent;
public pageIndex: number = 0;
public desde = 0;
public hasta = 10;
//Paginación de la tabla
next(event: PageEvent) {

if (event.pageIndex === this.pageIndex + 1) {
  this.desde = this.desde + this.pageSize;
  this.hasta = this.hasta + this.pageSize;
}
else if (event.pageIndex === this.pageIndex - 1) {
  this.desde = this.desde - this.pageSize;
  this.hasta = this.hasta - this.pageSize;
}
this.pageIndex = event.pageIndex;
}
changeRol(item,idRol){
  var obj={
    idRol,
    id:item.id
  }

    Swal.fire({
      title: 'Confirmación',
      html: ` <p> ¿Desea cambiar el rol de: <b>${item.nombres} ${item.apellidos}</b>? </p>`,
      icon: 'info',
      showCancelButton: true,
      confirmButtonColor: '#003399',
      cancelButtonColor: '#d33',
      cancelButtonText: 'cancelar',
      confirmButtonText: 'Aceptar'
    }).then((result) => {
      if (result.isConfirmed) {
        this.srvUsers.changeRol(obj,data=>{
          console.log(data)
          this.getUsers();
        })
      }
    });
}


}
