import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalAtendidoComponent } from './modal-atendido.component';

describe('ModalAtendidoComponent', () => {
  let component: ModalAtendidoComponent;
  let fixture: ComponentFixture<ModalAtendidoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModalAtendidoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalAtendidoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
