import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
import { PacientesService } from 'src/app/services/pacientes/pacientes.service';

@Component({
  selector: 'app-modal-atendido',
  templateUrl: './modal-atendido.component.html',
  styleUrls: ['./modal-atendido.component.css']
})
export class ModalAtendidoComponent implements OnInit {

  constructor( @Inject(MAT_DIALOG_DATA) public data: any,private srvPaciente: PacientesService,private toast :ToastrService,private modal:MatDialog) { }
  citaForm:FormGroup
  ngOnInit(): void {
    console.log(this.data)
    this.initCitaForm()
  }
  initCitaForm(){
    this.citaForm = new FormGroup({
      idPaciente: new FormControl(this.data.idPaciente),
      idCita: new FormControl(this.data.item.idCita),
      idDoctor: new FormControl(this.data.item.idDoctor),
      fechaCita: new FormControl(this.data.item.fechaCita),
      horaCita: new FormControl(this.data.item.horaCita),
      idEstadoCita:new FormControl(2),
      observaciones: new FormControl(this.data.item.observaciones),
      dictamen: new FormControl(this.data.item.dictamen),
      idDictamen: new FormControl(this.data.item.idDictamen),
      valoracionTelemedicina: new FormControl(this.data.item.valoracionTelemedicina),
      conclusiones: new FormControl(this.data.item.concluciones),
      resultadosExamenesPrevios: new FormControl(this.data.item.resultadosExamenesPrevios),
    })
  }

  saveCita(){
    console.log(this.citaForm.value)
    this.srvPaciente.updateCita(this.citaForm.value,data=>{
      console.log(data)
      this.toast.info('Realizado correctamente');
      this.modal.closeAll();
      // this.citaForm.reset();
      // this.citaForm.patchValue({idCita:0,idPaciente:this.data.item.id})
    })  
  }

}
