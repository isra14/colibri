export interface EncuestaAll {
    data:         Data;
    errors:       any[];
    hasError:     boolean;
    refreshToken: string;
}

export interface Data {
    table0: Table0[];
}

export interface Table0 {
    encuesta: Encuesta[];
}

export interface Encuesta {
    idCategoria: number;
    imagen?:     string;
    leyenda:     string;
    estado:      boolean;
    categoria:   string;
    orden?:      number;
    preguntas:   Pregunta[];
}

export interface Pregunta {
    obligatorio:    boolean;
    tipoPregunta:   string;
    pregunta:       string;
    idCategoria:    number;
    idNivel:        number;
    idPregunta:     number;
    idTipoPregunta: number;
    respuestas:     Respuesta[];
}

export interface Respuesta {
    idRespuesta:       number;
    idTipoRespuesta:   number;
    idEstilo:          number;
    ordenRespuesta:    number;
    respuesta:         string;
    seleccionada:      boolean;
    puntaje:           number;
    puntajeRespuesta:  number;
    estado:            boolean;
    estilo:            Estilo;
    idOpcionRespuesta: number;
}

export enum Estilo {
    Checkbox = "Checkbox",
    RadioButton = "Radio button",
}
