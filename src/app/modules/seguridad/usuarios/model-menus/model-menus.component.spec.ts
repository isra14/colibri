import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModelMenusComponent } from './model-menus.component';

describe('ModelMenusComponent', () => {
  let component: ModelMenusComponent;
  let fixture: ComponentFixture<ModelMenusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModelMenusComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModelMenusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
