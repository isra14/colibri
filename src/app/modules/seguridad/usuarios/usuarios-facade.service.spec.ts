import { TestBed } from '@angular/core/testing';

import { UsuariosFacadeService } from './usuarios-facade.service';

describe('UsuariosFacadeService', () => {
  let service: UsuariosFacadeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(UsuariosFacadeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
