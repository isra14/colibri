import { Component, Input, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { PageEvent } from '@angular/material/paginator';
import { PinPermissionFacadeService } from '../pin-permission/pin-permission-facade.service';
import { UserLocationFacadeService } from '../user-location/user-location-facade.service';
import { UsuariosFacadeService } from '../usuarios/usuarios-facade.service';

@Component({
  selector: 'app-user-table',
  templateUrl: './user-table.component.html',
  styleUrls: ['./user-table.component.css']
})
export class UserTableComponent implements OnInit {
  //IdAccion === 1 es setear usuario para pin permisos
  //IdAccion === 2 es setear usuario para usuario ubicacion

  @Input() idAccion: number;
  public parametrosBusqueda = ['nombre', 'usuario'];

  //Variable del tipo FormGroup, se inicializa en el constructor
  public filtro = new FormControl('');

  //Paginacion
  public pageSize = 10;
  public page = 0;
  public pageEvent!: PageEvent;
  public pageIndex: number = 0;
  public desde = 0;
  public hasta = 10;
  
  constructor(public usuariosFacade: UsuariosFacadeService, public PinPermissionFacade: PinPermissionFacadeService, public usuarioUbicacionFacade: UserLocationFacadeService) {
    this.usuariosFacade.MostrarUsuarios('');

  }

  ngOnInit(): void {
  }

  next(event: PageEvent) {

    if (event.pageIndex === this.pageIndex + 1) {
      this.desde = this.desde + this.pageSize;
      this.hasta = this.hasta + this.pageSize;
    }
    else if (event.pageIndex === this.pageIndex - 1) {
      this.desde = this.desde - this.pageSize;
      this.hasta = this.hasta - this.pageSize;
    }
    this.pageIndex = event.pageIndex;
  }

  seleccionarUsuario(item) {
    if (this.idAccion === 1) {
      this.PinPermissionFacade.setearUsuario(item);
    }

    if (this.idAccion === 2) {
      this.usuarioUbicacionFacade.setearUsuario(item);
    }
  }

}
