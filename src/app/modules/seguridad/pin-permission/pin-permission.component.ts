import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup } from '@angular/forms';
import { Subscription } from 'rxjs';
import { permisosPinUsuario } from 'src/app/Model/Seguridad/permisos.Interface';
import { PinPermissionFacadeService } from './pin-permission-facade.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-pin-permission',
  templateUrl: './pin-permission.component.html',
  styleUrls: ['./pin-permission.component.scss']
})
export class PinPermissionComponent implements OnInit, OnDestroy {
  public filter = new FormControl('');
  public permisosPinUsuario = new FormArray([]);
  public suscripciones: Subscription = new Subscription();
  
  constructor(public PinPermissionFacade: PinPermissionFacadeService) {
    this.PinPermissionFacade.mostrarTipoPermisos('');
    this.suscripciones.add(
    this.PinPermissionFacade.responsePermisos$.subscribe((result) => {
      if (result.length > 0) {
        this.setearPermisos(result);
      }
    })
    );

    this.suscripciones.add(
      this.PinPermissionFacade.responsePermisosPinUsuario$.subscribe((result: any[]) => {
        this.reiniciarValor();
        if (result.length > 0) {
          this.cambiarEstadoPermisos(result);
        }
      })
    );
  }

  ngOnInit(): void {
  }

  ngOnDestroy() {
    this.suscripciones.unsubscribe();
    this.PinPermissionFacade.resetearUsuario();
  }

  setearPermisos(permisosArray: any[]) {
    permisosArray.forEach((p) => {
      let permisos = new FormGroup({
        id: new FormControl(0),
        idPermisoPin: new FormControl(p.idPermisoPin),
        descripcion: new FormControl(p.descripcion),
        asignacion: new FormControl(false),
      });

      this.permisosPinUsuario.push(permisos);
    });

  }

  cambiarEstadoPermisos(permisosUsuario: permisosPinUsuario[]) {
    permisosUsuario.forEach((u) => {
      this.permisosPinUsuario.controls.forEach((p) => {
        if (u.idPermisoPin === p.value.idPermisoPin) {
          p.get('id').setValue(u.id);
          p.get('asignacion').setValue(true);
        }
      });
    });
  }

  asignar(item) {
    if (item.value.asignacion === false) {
      this.swalConfirmacion(1, item.value);
    } else {
      this.swalConfirmacion(2, item.value);
    }
  }

  swalConfirmacion(idAccion, item) {
    Swal.fire({
      title: 'Confirmación',
      html: ` <p> ¿Esta seguro quiere ${(idAccion === 1) ? 'Agregar' : 'quitar'} el permiso: <b>${item.descripcion}</b>? </p>`,
      icon: 'question',
      showCancelButton: true,
      confirmButtonColor: '#003399',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Confirmar',
      cancelButtonText: 'Cancelar'
    }).then((result) => {
      if (result.isConfirmed) {
        if (idAccion === 1) {
          this.PinPermissionFacade.insertarPermisos({
            evento: {
              accion: "nuevo",
              idAccion: "1",
              usuario: "",
              idPermisoPin: item.idPermisoPin
            }
          });
        }
        if (idAccion === 2) {
          this.PinPermissionFacade.elimarPermiso(`${item.id}`);
        }


      } else {
        this.permisosPinUsuario.controls.forEach((p) => {
          if (p.get('idPermisoPin').value === item.idPermisoPin) {
            p.get('asignacion').setValue(item.asignacion);
          }
        });
      }
    });
  }

  reiniciarValor() {
    this.permisosPinUsuario.controls.forEach((p) => {
      p.get('asignacion').setValue(false);
    });
  }

  

}
