import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PinPermissionComponent } from './pin-permission.component';

describe('PinPermissionComponent', () => {
  let component: PinPermissionComponent;
  let fixture: ComponentFixture<PinPermissionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PinPermissionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PinPermissionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
