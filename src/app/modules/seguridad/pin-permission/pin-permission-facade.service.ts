import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, EMPTY, Observable } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { DataApi } from 'src/app/Model/dataApi';
import { permisosPin, permisosPinUsuario } from 'src/app/Model/Seguridad/permisos.Interface';
import { usuarioInterface } from 'src/app/Model/Seguridad/usuarios.Interface';
import { DataApiService } from 'src/app/services/data-api.service';
import { ToastrServiceLocal } from 'src/app/services/toast/toastr.service';
import { MensajesHttpService } from 'src/app/utils/mensajesHttp/mensajes-http.service';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PinPermissionFacadeService {
  
  private Usuarios$ = new BehaviorSubject<usuarioInterface>({});
  public responseUsuarios$: Observable<usuarioInterface> = this.Usuarios$.asObservable();

  private UsuarioAsignado$ = new BehaviorSubject<boolean>(false);
  public responseUsuarioAsignado$: Observable<boolean> = this.UsuarioAsignado$.asObservable();

  private Permisos$ = new BehaviorSubject<permisosPin[]>([]);
  public responsePermisos$: Observable<permisosPin[]> = this.Permisos$.asObservable();

  private PermisosPinUsuario$ = new BehaviorSubject<permisosPinUsuario[]>([]);
  public responsePermisosPinUsuario$: Observable<permisosPinUsuario[]> = this.PermisosPinUsuario$.asObservable();

  private Cargando$ = new BehaviorSubject<boolean>(false);
  public responseCargando$: Observable<boolean> = this.Cargando$.asObservable();

  private Action$ = new BehaviorSubject<any>({});
  public responseAction$: Observable<any> = this.Action$.asObservable();

  constructor(public _toast: ToastrServiceLocal, public _dataApi: DataApiService,
    public _mensajesHttp: MensajesHttpService) { };

  setearUsuario(usuario) {
    this.Usuarios$.next(usuario);
    this.UsuarioAsignado$.next(true);
    this.mostrarPermisosPinUsuario(`${usuario.Usuario}`);
  }

  resetearUsuario() {
    this.Usuarios$.next({});
    this.UsuarioAsignado$.next(false);

  }


  mostrarTipoPermisos(params?: any) {
    this.Permisos$.next([]);
    this.Cargando$.next(true);
    const request$ = this._dataApi.GetDataApi(`${environment.site}/api/sec/${environment.processEnv}/v1/pinpermission`,).pipe(
      tap((result: DataApi | any) => {
        if (result.hasError === false) {
          this.Permisos$.next(result.data.table0);
        }
        this.Cargando$.next(false);
      }),
      catchError((error: HttpErrorResponse) => {
        this._mensajesHttp.mostrarErrorHttp(error, 'Ocurrio un error al mostrar los tipos de permisos', '');
        this.Cargando$.next(false);
        return EMPTY;
      })
    );
    return request$.subscribe();
  }

  mostrarPermisosPinUsuario(params?: any) {
    this.PermisosPinUsuario$.next([]);

    this.Cargando$.next(true);
    const request$ = this._dataApi.GetDataApi(`${environment.site}/api/sec/${environment.processEnv}/v1/user-pin-permission/`,).pipe(
      tap((result: DataApi | any) => {
        if (result.hasError === false) {
          this.PermisosPinUsuario$.next(result.data.table0);
        }
        this.Cargando$.next(false);
      }),
      catchError((error: HttpErrorResponse) => {
        this._mensajesHttp.mostrarErrorHttp(error, 'Ocurrio un error al mostrar los permisos del usuario', '');
        this.Cargando$.next(false);
        return EMPTY;
      })
    );
    return request$.subscribe();
  }

  insertarPermisos(params: any) {
    params.evento.usuario = this.Usuarios$.value.Usuario;
    this.Cargando$.next(true);
    this._toast.mensajeLoading('Cargando', 'Procesando la información');

    const request$ = this._dataApi.PutDataApi(`${environment.site}/api/sec/${environment.processEnv}/v1/user-pin-permission/`, params).pipe(
      tap((result: DataApi | any) => {
        if (result.hasError === false) {
          this._toast.mensajeSuccess(`${result.data.table0[0].descripcion}`,'')
          this.agregarPermisoUsuario(result.data.table1[0]);
        }
        this._toast.clearToasts();
        this.Cargando$.next(false);
      }),
      catchError((error: HttpErrorResponse) => {
        this._toast.clearToasts();
        this._mensajesHttp.mostrarErrorHttp(error, 'Ocurrio un error al agregar el permiso al usuario', '');
        this.Cargando$.next(false);
        return EMPTY;
      })
    );
    return request$.subscribe();
  }

  elimarPermiso(params: any) {
    this.Cargando$.next(true);
    this._toast.mensajeLoading('Cargando', 'Procesando la información');

    const request$ = this._dataApi.DeleteDataApi(`${environment.site}/api/sec/${environment.processEnv}/v1/user-pin-permission/`, params,).pipe(
      tap((result: DataApi | any) => {
        if (result.hasError === false) {
          this._toast.mensajeSuccess(`${result.data.table0[0].descripcion}`, '')
          this.mostrarPermisosPinUsuario(`${this.Usuarios$.value.Usuario}`);
        }
        this._toast.clearToasts();
        this.Cargando$.next(false);
      }),
      catchError((error: HttpErrorResponse) => {
        this._toast.clearToasts();
        this._mensajesHttp.mostrarErrorHttp(error, 'Ocurrio un error al eliminar el permiso al usuario', '');
        this.Cargando$.next(false);
        return EMPTY;
      })
    );
    return request$.subscribe();
  }

  agregarPermisoUsuario(item) {
    let permisosPin: any[] = [];
    permisosPin = this.PermisosPinUsuario$.value;
    permisosPin.push(item);
    this.PermisosPinUsuario$.next([]);
    this.PermisosPinUsuario$.next(permisosPin);
  }

  

}
