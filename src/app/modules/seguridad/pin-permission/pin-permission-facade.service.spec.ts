import { TestBed } from '@angular/core/testing';

import { PinPermissionFacadeService } from './pin-permission-facade.service';

describe('PinPermissionFacadeService', () => {
  let service: PinPermissionFacadeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PinPermissionFacadeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
