import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';


//Componentes
import { SeguridadRoutingModule } from './seguridad-routing.module';
import { MenusComponent } from './menus/menus.component';
import { PipeModule } from '../../core/pipe/pipe.module';
import { SharedModule } from '../../shared/shared.module';
import { NuevoMenuComponent } from './menus/nuevo-menu/nuevo-menu.component';
import { SubMenuComponent } from './menus/sub-menu/sub-menu.component';
import { ModalInformationComponent } from './menus/modal-information/modal-information.component';
import { TableMenusComponent } from './menus/table-menus/table-menus.component';
import { PerfilesComponent } from './perfiles/perfiles.component';
import { ModalNuevoPerfilComponent } from './perfiles/modal-nuevo-perfil/modal-nuevo-perfil.component';
import { NuevoUsuarioModalComponent } from './usuarios/nuevo-usuario-modal/nuevo-usuario-modal.component';
import { TablePerfilesComponent } from './perfiles/table-perfiles/table-perfiles.component';
import { UsuariosComponent } from './usuarios/usuarios.component';
import { ModalPerfilesComponent } from './usuarios/modal-perfiles/modal-perfiles.component';
import { AsinarMenusModalComponent } from './asinar-menus-modal/asinar-menus-modal.component';
import { PersonalizacionComponent } from './personalizacion/personalizacion.component';

//Angular Material
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatStepperModule } from '@angular/material/stepper';
import { MatRadioModule } from '@angular/material/radio';
import { MatBottomSheetModule } from '@angular/material/bottom-sheet';
import { ModelMenusComponent } from './usuarios/model-menus/model-menus.component';
import { MatTabsModule } from '@angular/material/tabs';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatListModule } from '@angular/material/list';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatDialogModule } from '@angular/material/dialog';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatSelectModule } from '@angular/material/select';
import { ModulosComponent } from './modulos/modulos.component';
import { PinPermissionComponent } from './pin-permission/pin-permission.component';
import { UserLocationComponent } from './user-location/user-location.component';
import { UserTableComponent } from './user-table/user-table.component';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
@NgModule({
  declarations: [MenusComponent, NuevoMenuComponent, SubMenuComponent, ModalInformationComponent, 
    TableMenusComponent, PerfilesComponent, ModalNuevoPerfilComponent, UsuariosComponent, NuevoUsuarioModalComponent, 
    TablePerfilesComponent, ModalPerfilesComponent, ModelMenusComponent, AsinarMenusModalComponent, 
    PersonalizacionComponent, ModulosComponent, PinPermissionComponent, UserLocationComponent, UserTableComponent],
  imports: [
    CommonModule,
    MatTabsModule,
    MatSlideToggleModule,
    MatToolbarModule,
    MatExpansionModule,
    MatBottomSheetModule,
    MatRadioModule,
    MatCheckboxModule,
    MatListModule,
    SeguridadRoutingModule,
    ReactiveFormsModule,
    MatStepperModule,
    MatSelectModule,
    MatFormFieldModule,
    MatDialogModule,
    MatInputModule,
    PipeModule,
    MatTooltipModule,
    SharedModule,
    MatPaginatorModule,
    MatCardModule,
    MatDialogModule,
    MatButtonModule,
    MatIconModule,
    MatAutocompleteModule
  ]
})
export class SeguridadModule { }
