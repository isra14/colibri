import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { DataApi } from '../../../Model/dataApi';
import { personalizacionInterface } from '../../../Model/Seguridad/personalizacion.Interface';

import { DataApiService } from '../../../services/data-api.service';
import { ToastrServiceLocal } from '../../../services/toast/toastr.service';
import { MensajesHttpService } from '../../../utils/mensajesHttp/mensajes-http.service';
import { BehaviorSubject, EMPTY, Observable, Subject } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { SeguridadService } from 'src/app/services/seguridad/seguridad.service';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PersonalizacionFacadeService {

  constructor(private _mensajesHttp: MensajesHttpService,
    private _toast: ToastrServiceLocal, private _dataApi: SeguridadService) {
      // this.MenusUsuario$.next([]);
      // console.log('entro');
    }

  //Variable para el control de los cargando
  private Cargando$ = new BehaviorSubject<boolean>(true);
  public responseCargando$: Observable<boolean> = this.Cargando$.asObservable();

  //Variable para el control de la respuesta de las acciones
  private Action$ = new Subject<any>();
  public responseAction$: Observable<any> = this.Action$.asObservable();

  private MenusUsuario$ = new BehaviorSubject<personalizacionInterface[]>([]);
  public responseMenusUsuarios$:Observable<personalizacionInterface[]> = this.MenusUsuario$.asObservable();

  insertPersonalizacion(params: any) {
    this.Cargando$.next(true);
    this._toast.mensajeLoading('Cargando', 'Procesando la información');
    const request$ = this._dataApi.PutDataApi(`/${environment.site}/api/sec/${environment.processEnv}/v1/admin/user/dashboard`, params).pipe(
      tap((result: DataApi | any) => {
        this._toast.clearToasts();
        this.Action$.next(result);
        this.Cargando$.next(false);
        this._toast.mensajeSuccess('Sus preferencias se guardaron con exito', '')
      }),
      catchError((error: HttpErrorResponse) => {
        this._toast.clearToasts();
        this.Action$.next({});
        this._mensajesHttp.mostrarErrorHttp(error, 'Ocurrio un error al insertar sus menus personalizados', '');
        this.Cargando$.next(false);
        return EMPTY;
      })
    );

    return request$.subscribe();
  }

  mostrarMenusPersonalizados(params: any) {
    this.MenusUsuario$.next([]);
    this.Cargando$.next(true);
    // this.MenusUsuario$.next(this.dashBoard);
    const request$ = this._dataApi.GetDataApi(`/${environment.site}/api/sec/${environment.processEnv}/v1/admin/user/dashboard`, params).pipe(
      tap((result: DataApi | any) => {
        // console.log(result);
        this.MenusUsuario$.next(result.data.Table0);
        this.Cargando$.next(false);
      }),
      catchError((error: HttpErrorResponse) => {
        this._toast.clearToasts();
        this._mensajesHttp.mostrarErrorHttp(error, 'Ocurrio un error al mostrar sus menus personalizados', '');
        this.Cargando$.next(false);
        return EMPTY;
      })
    );

    return request$.subscribe();
  }

  deletePersonalizacion(params: any) {
    this.Cargando$.next(true);
    // this._toast.mensajeLoading('Cargando', 'Procesando la información');
    const request$ = this._dataApi.DeleteDataApi(`/${environment.site}/api/sec/${environment.processEnv}/v1/admin/user/dashboard`, params).pipe(
      tap((result: DataApi | any) => {
        // this._toast.clearToasts();
        // console.log(result);
        this.Action$.next(result);
        this.Cargando$.next(false);
      }),
      catchError((error: HttpErrorResponse) => {
        this._toast.clearToasts();
        this.Action$.next({});
        this._mensajesHttp.mostrarErrorHttp(error, 'Ocurrio un error al insertar sus menus personalizados', '');
        this.Cargando$.next(false);
        return EMPTY;
      })
    );

    return request$.subscribe();
  }

  setearMenusUsuario(){
    this.MenusUsuario$.next([]);
  }

  public dashBoard:personalizacionInterface[] = [
    { id: 1, idTipoElemento: 1, nombreElemento: "Menus", idElemento: 319 },
    { id: 2, idTipoElemento: 1, nombreElemento: "Menus", idElemento: 321 },
    { id: 3, idTipoElemento: 1, nombreElemento: "Menus", idElemento: 322 },
    { id: 4, idTipoElemento: 1, nombreElemento: "Menus", idElemento: 323 }

  ];

}
