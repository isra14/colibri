import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { menuInterface } from '../../../Model/Seguridad/menu.Interface';
import { personalizacionInterface } from '../../..//Model/Seguridad/personalizacion.Interface';
import { Subscription } from 'rxjs';
import { MenusFacadeService } from '../menus/menus-facade.service';
import { PersonalizacionFacadeService } from './personalizacion-facade.service';

@Component({
  selector: 'app-personalizacion',
  templateUrl: './personalizacion.component.html',
  styleUrls: ['./personalizacion.component.scss']
})
export class PersonalizacionComponent implements OnInit, OnDestroy {

  public step = 0;
  public menusArray: menuInterface[] = [];
  public suscripciones: Subscription = new Subscription();
  public formCheckedMenus: FormGroup | any;
  public formBuilder!: FormBuilder;
  public menusUsuario!: personalizacionInterface[];
  // public menusAgregar: any[] = [];

  constructor(public menusFacade: MenusFacadeService, public personalizacionFacade: PersonalizacionFacadeService) {
    this.menusFacade.MostrarMenuUsuario('');
    this.formCheckedMenus = new FormGroup({});
    this.personalizacionFacade.setearMenusUsuario();

    this.suscripciones.add(this.menusFacade.responseMenus$.subscribe((result) => {
      if (result.length != 0) {
        this.menusArray = result;
        this.formBuilderDynamic(result);
      }
    }));
    this.suscripciones.add(this.personalizacionFacade.responseMenusUsuarios$.subscribe((result:any) => {
      if (result.length != 0) {
        this.menusUsuario = result;
        this.setearValores();
      }
    }));
  }

  ngOnDestroy(): void{
    this.suscripciones.unsubscribe();
  }

  formBuilderDynamic(menuArray:any) {
    for (const i in menuArray) {
      //Agregar un control para los menus padre
      this.formCheckedMenus.addControl(`${menuArray[i].idMenu}`, new FormControl(false, Validators.required));
      //Agregar controles para los menus hijos
      for (const j in menuArray[i].hijos) {
        this.formCheckedMenus.addControl(`${menuArray[i].hijos[j].idMenu}`, new FormControl(false, Validators.required));
      }
    }

    this.personalizacionFacade.mostrarMenusPersonalizados({});

    // console.log(this.formCheckedMenus);
  }

  setearValores() {
    for (const i in this.menusUsuario) {
      this.formCheckedMenus.get(`${this.menusUsuario[i].idElemento}`).setValue(true);
    }
  }

  //Guardar las preferencias de los menus
  guardarMenuPreferencia() {
    let menusQuitar: any[] = [];
    let menusAgregar: any[] = [];

    for (const i in this.menusArray) {

      if (this.formCheckedMenus.get(String(this.menusArray[i].idMenu)).value === true) {
        console.log(this.menusArray[i].idMenu);
        menusAgregar.push({ idTipoElemento: 1, idElemento: this.menusArray[i].idMenu });

      }
      else if (this.formCheckedMenus.get(String(this.menusArray[i].idMenu)).value === false) {

        menusQuitar.push({ idTipoElemento: 1, idElemento: this.menusArray[i].idMenu });

      }
      //Agregar controles para los menus hijos
      for (const j in this.menusArray[i].hijos) {

        if (this.formCheckedMenus.get(String(this.menusArray[i].hijos[j].idMenu)).value === true) {
          menusAgregar.push({ idTipoElemento: 1, idElemento: this.menusArray[i].hijos[j].idMenu });

        }
        else if (this.formCheckedMenus.get(String(this.menusArray[i].hijos[j].idMenu)).value === false){

          menusQuitar.push({ idTipoElemento: 1, idElemento: this.menusArray[i].hijos[j].idMenu });

        }

      }

    }

    // let newMenusAgregar: any[] = [];
    // console.log(menusAgregar);
    // console.log(this.menusUsuario);

    for(const i in this.menusUsuario){
      for(const j in menusAgregar){
        if(menusAgregar[j].idElemento === this.menusUsuario[i].idElemento){
          menusAgregar.splice(Number(j), 1);
        }
      }
    }

    let paramsInsertar = { jsonDashboard: JSON.stringify(menusAgregar)};
    let paramsEliminar = { jsonDashboard: JSON.stringify(menusQuitar)};
    // console.log(paramsEliminar);
    // console.log(paramsInsertar);

    this.personalizacionFacade.insertPersonalizacion(paramsInsertar);
    this.personalizacionFacade.deletePersonalizacion(paramsEliminar);


  }


  ngOnInit(): void {
  }

  setStep(index: number) {
    this.step = index;
  }

  nextStep() {
    this.step++;
  }

  prevStep() {
    this.step--;
  }

}
