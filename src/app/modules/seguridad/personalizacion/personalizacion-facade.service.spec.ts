import { TestBed } from '@angular/core/testing';

import { PersonalizacionFacadeService } from './personalizacion-facade.service';

describe('PersonalizacionFacadeService', () => {
  let service: PersonalizacionFacadeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PersonalizacionFacadeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
