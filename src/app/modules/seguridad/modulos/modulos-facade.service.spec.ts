import { TestBed } from '@angular/core/testing';

import { ModulosFacadeService } from './modulos-facade.service';

describe('ModulosFacadeService', () => {
  let service: ModulosFacadeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ModulosFacadeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
