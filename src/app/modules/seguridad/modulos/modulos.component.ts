import { Component, OnInit } from "@angular/core";
import { FormArray, FormControl, FormGroup, Validators } from "@angular/forms";
import { MatDialog } from "@angular/material/dialog";
import { Subscription } from "rxjs";
import { mosaicInterface } from "src/app/Model/Seguridad/mosaic.interface";
import { PaginatorService } from "src/app/services/paginator.service";
import { ToastrServiceLocal } from "src/app/services/toast/toastr.service";
import { ModulosFacadeService } from "./modulos-facade.service";
import Swal from "sweetalert2";

@Component({
  selector: "app-modulos",
  templateUrl: "./modulos.component.html",
  styleUrls: ["./modulos.component.scss"],
})
export class ModulosComponent implements OnInit {
  //Variable del tipo FormGroup, se inicializa en el constructor
  public filtro: FormGroup;
  public moduloForm: FormGroup;
  public codigoCss = new FormArray([]);
  public estilos: any = {};

  public parametrosBusqueda = ["descripcion", "proveedor"];
  public suscripciones: Subscription = new Subscription();

  constructor(
    public modulosFacade: ModulosFacadeService,
    public paginatorService: PaginatorService,
    public dialog: MatDialog,
    public _toast: ToastrServiceLocal
  ) {
    this.modulosFacade.mostrarModulo("");

    this.filtro = new FormGroup({
      filter: new FormControl({ value: "", disabled: false }),
      idEstado: new FormControl(""),
    });

    this.moduloForm = new FormGroup({
      id: new FormControl(0),
      modulo: new FormControl("", [Validators.required]),
      imagen: new FormControl("", [Validators.required]),
      titulo: new FormControl("", [Validators.required]),
      estado: new FormControl(true),
      css: new FormControl(""),
      descripcion: new FormControl(""),
    });
  }

  agregarValoresCss() {
    let css = new FormGroup({
      clase: new FormControl("", [Validators.required]),
      value: new FormControl("", [Validators.required]),
    });

    this.codigoCss.push(css);
  }

  setearValoresCss(item) {
    let css = new FormGroup({
      clase: new FormControl(item.clase, [Validators.required]),
      value: new FormControl(item.value, [Validators.required]),
    });

    this.codigoCss.push(css);
  }

  ngOnInit(): void {}

  openModalPuntoVenta(template, value?) {
    if (value) {
      this.setearValor(value);
    } else {
      this.resetearValor();
    }
    const dialogRef = this.dialog.open(template, {
      width: "900px",
      panelClass: "app-full-bleed-dialog",
    });

    this.suscripciones.add(dialogRef.afterClosed().subscribe((result) => {}));
  }

  setearValor(modulo: mosaicInterface) {
    // let cssObject;
    //   cssObject = JSON.parse(modulo?.css );
    //   for (const i in cssObject) {
    //     this.setearValoresCss(cssObject[i]);
    //   }
    
    this.moduloForm.patchValue({
      id: modulo.idModule === undefined ? 0 : modulo.idModule,
      modulo: modulo.Module,
      imagen: modulo.image,
      titulo: modulo.title,
      estado: modulo.idStatus,
      css: "",
      descripcion: modulo.description,
    });
    console.log(this.moduloForm.value);
  }

  resetearValor() {
    this.moduloForm.patchValue({
      id: 0,
      modulo: "",
      imagen: "",
      titulo: "",
      estado: "",
      css: "",
      descripcion: "",
    });
  }

  agregarEstilos() {

    let clasesCss:any[] = [];
    this.codigoCss.value.forEach((res) => {
      let bodyObject = {}
      bodyObject[res.clase.replace(/ /g, "")] = String(res.value);
      clasesCss.push(bodyObject);
    });

    this.moduloForm.get('css').setValue(JSON.stringify(clasesCss));
    if (this.moduloForm.value.id === 0) {
      this.moduloForm.get('estado').setValue(true);
      this.modulosFacade.NuevoModulo({ valor: [this.moduloForm.value] });
    }else{
      this.modulosFacade.actualizarModulo({ valor: [this.moduloForm.value] });
    }
    this.suscripciones.add(this.modulosFacade.responseAction$.subscribe((result) => {
      if (result.hasError === false) {
        this.dialog.closeAll();
        this.modulosFacade.mostrarModulo('');
        this.moduloForm.reset();
        this.codigoCss.controls.splice(-1);

      }
    }));
  }

  guardarModulo() {
    if (this.moduloForm.invalid || this.codigoCss.invalid) {
      this.moduloForm.markAllAsTouched();
      this.codigoCss.markAllAsTouched();
      this._toast.mensajeWarning(
        "",
        "Es necesario completar los campos obligatorios"
      );
      return;
    } else {
      this.agregarEstilos();
    }
  }

  eliminarObjeto(i) {
    this.codigoCss.controls.splice(i, 1);
  }

  eliminarModulo(item: mosaicInterface) {
    Swal.fire({
      title: "Confirmación",
      html: ` <p> ¿Esta seguro quiere eliminar el modulo: <b>${item.Module}</b>? </p>`,
      icon: "question",
      showCancelButton: true,
      confirmButtonColor: "#003399",
      cancelButtonColor: "#d33",
      confirmButtonText: "Confirmar",
      cancelButtonText: "Cancelar",
    }).then((result) => {
      if (result.isConfirmed) {
        this.modulosFacade.eliminarModulo({
          valor: [{ id: item.idModule }],
        });
        this.suscripciones.add(
          this.modulosFacade.responseAction$.subscribe((result) => {
            if (result === false) {
              this.modulosFacade.mostrarModulo("");
            }
          })
        );
      }
    });
  }

  
}
