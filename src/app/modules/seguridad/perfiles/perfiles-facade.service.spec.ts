import { TestBed } from '@angular/core/testing';

import { PerfilesFacadeService } from './perfiles-facade.service';

describe('PerfilesFacadeService', () => {
  let service: PerfilesFacadeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PerfilesFacadeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
