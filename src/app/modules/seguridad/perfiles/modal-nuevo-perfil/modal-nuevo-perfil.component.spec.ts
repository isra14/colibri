import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalNuevoPerfilComponent } from './modal-nuevo-perfil.component';

describe('ModalNuevoPerfilComponent', () => {
  let component: ModalNuevoPerfilComponent;
  let fixture: ComponentFixture<ModalNuevoPerfilComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalNuevoPerfilComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalNuevoPerfilComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
