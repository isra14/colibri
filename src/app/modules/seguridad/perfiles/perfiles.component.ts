import { Component, OnInit, OnDestroy, ChangeDetectorRef } from '@angular/core';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import { MenusFacadeService } from '../menus/menus-facade.service';
import { Subscription } from 'rxjs';
import { ModalNuevoPerfilComponent } from './modal-nuevo-perfil/modal-nuevo-perfil.component';
import { SweetAlertService } from '../../../utils/sweetAlert/sweet-alert.service';
import { PerfilesFacadeService } from './perfiles-facade.service';
import { permisosInterface } from '../../../Model/Seguridad/permisos.Interface';
import { AsinarMenusModalComponent } from '../asinar-menus-modal/asinar-menus-modal.component';
import { MatDialog } from '@angular/material/dialog';
import { PermisosValidarService } from 'src/app/services/seguridad/permisos-validar.service';

@Component({
  selector: 'app-perfiles',
  templateUrl: './perfiles.component.html',
  styleUrls: ['./perfiles.component.css']
})
export class PerfilesComponent implements OnInit, OnDestroy {
  public permisos:permisosInterface;

  private subscripciones: Subscription = new Subscription();
  public data: any;
  public verMenus: boolean ;
  public tipoFiltro: string = "1";

  constructor(public router: Router, public menusFacade: MenusFacadeService,
    private cdRef: ChangeDetectorRef,
    public dialog: MatDialog,
    public perfilFacade: PerfilesFacadeService,
    public sweetAlert: SweetAlertService,
    public validarService: PermisosValidarService
  ) {
    this.permisos = this.validarService.validate('Perfiles');
    
    this.menusFacade.currentUrl = this.router.url;
    this.perfilFacade.MostrarPerfiles('');

    this.subscripciones.add(this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        this.menusFacade.previousUrl = this.menusFacade.currentUrl;
        // console.log(this.menusFacade.previousUrl);
        this.menusFacade.currentUrl = event.url;
      };
    }));

    this.subscripciones.add(this.perfilFacade.responseSeleccionado$.subscribe((result)=>{
        this.menusPerfil(result);
    }));

    this.verMenus = false;
  }

  ngOnDestroy(): void {
    this.subscripciones.unsubscribe();
  }

  ngAfterViewChecked() {
    this.cdRef.detectChanges();
  }


  ngOnInit(): void {
  }

  menusPerfil(item:any) {
    this.verMenus = item;
  }

  eliminarPerfil(item:any) {
    console.log(item);
    this.sweetAlert.mensajeConConfirmacion('Mensaje Confirmación', `¿Esta seguro que desea eleminar el perfil ${item.Nombre}?`, 'warning')
      .then((res: boolean) => {
        if (res) {
          this.perfilFacade.EliminarPerfil({idPerfil: item.idPerfil});
        }
      });
  }
  openDialog(data:any) {
    const dialogRef = this.dialog.open(ModalNuevoPerfilComponent, {
      width: '60vw',
      maxWidth: '80vw',
      data: data,
      panelClass: "app-full-bleed-dialog",

    });
  }

  consumirMenus(){
    this.menusFacade.MostrarPerfilesMenu(this.menusFacade.itemMenus.IdPerfil);
  }

  dialogMenus() {
    const dialogRef = this.dialog.open(AsinarMenusModalComponent, {
      width: '80vw',
      maxWidth: '100vw',
      panelClass: 'app-full-bleed-dialog',
      disableClose: true
    });
  }

}
