import { TestBed } from '@angular/core/testing';

import { MenusFacadeService } from './menus-facade.service';

describe('MenusFacadeService', () => {
  let service: MenusFacadeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MenusFacadeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
