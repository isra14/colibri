import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MenusFacadeService } from '../menus-facade.service';
import { ActivatedRoute, Router, NavigationStart } from '@angular/router';
import { ModalInformationComponent } from '../modal-information/modal-information.component';
import { Subscription } from 'rxjs';
import { PageEvent } from '@angular/material/paginator';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-sub-menu',
  templateUrl: './sub-menu.component.html',
  styleUrls: ['./sub-menu.component.css']
})
export class SubMenuComponent implements OnInit, OnDestroy {

  public suscription: Subscription = new Subscription();
  public idMenuPadre:number;
  public nombreMenu:string;
  public filtro: FormGroup;
  public arrayMenus: any[] = [];

  //Paginacion
  public pageSize = 10;
  public page = 0;
  public pageEvent!: PageEvent;
  public pageIndex: number = 0;
  public desde = 0;
  public hasta = 10;
  public parametrosBusqueda = ['texto', 'descripcion'];

  constructor(public menusFacade: MenusFacadeService, public activatedRoute:ActivatedRoute,
    public dialog: MatDialog, public router: Router) {

    this.idMenuPadre = Number(this.activatedRoute.snapshot.paramMap.get('id'));
    this.nombreMenu = this.activatedRoute.snapshot.paramMap.get('nombre')!;
    this.suscription.add(this.menusFacade.responseMenus$.subscribe((result) => {
      if(result.length != 0){
        this.subMenuConstruir(result);
      }
    }));

    this.menusFacade.MostrarMenu({});
    this.filtro = new FormGroup({
      filter: new FormControl({ value: '', disabled: false })
    });


   }

  ngOnInit(): void {

  }

  ngOnDestroy():void{
    this.suscription.unsubscribe();
  }

  subMenuConstruir(menus:any) {
    for (let i = 0; i < menus.length; i++) {
      if (menus[i].idMenu === this.idMenuPadre){
        this.arrayMenus = menus[i].hijos;
      }
    }
  }

  next(event: PageEvent) {

    if (event.pageIndex === this.pageIndex + 1) {
      this.desde = this.desde + this.pageSize;
      this.hasta = this.hasta + this.pageSize;
    }
    else if (event.pageIndex === this.pageIndex - 1) {
      this.desde = this.desde - this.pageSize;
      this.hasta = this.hasta - this.pageSize;
    }
    this.pageIndex = event.pageIndex;
  }

  openDialog(data:any) {
    const dialogRef = this.dialog.open(ModalInformationComponent, {
      width: '80vw',
      maxWidth: '100vw',
      data: data
    });
  }

}
