import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MenusComponent } from './menus/menus.component';
import { SubMenuComponent } from './menus/sub-menu/sub-menu.component';
import { NuevoMenuComponent } from './menus/nuevo-menu/nuevo-menu.component';
import { PerfilesComponent } from './perfiles/perfiles.component';
import { UsuariosComponent } from './usuarios/usuarios.component';
import { PersonalizacionComponent } from './personalizacion/personalizacion.component';
import { ModulosComponent } from './modulos/modulos.component';
import { PinPermissionComponent } from './pin-permission/pin-permission.component';
import { UserLocationComponent } from './user-location/user-location.component';


const routes: Routes = [
  {
    path: 'menus',
    component: MenusComponent
  },
  {
    path: 'usuarios',
    component: UsuariosComponent
  },
  {
    path: 'perfiles',
    component: PerfilesComponent
  },
  {
    path: 'personalizacion',
    component: PersonalizacionComponent
  },
  {
    path: 'subMenu/:id/:nombre',
    component: SubMenuComponent
  },
  {
    path: 'nuevoMenu/:idMenu/:idPadre',
    component: NuevoMenuComponent
  },
  {
    path: 'modulos',
    component: ModulosComponent
  },
  {
    path: 'pinPermission',
    component: PinPermissionComponent
  },
  {
    path: 'userLocation',
    component: UserLocationComponent
  },
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SeguridadRoutingModule { }
