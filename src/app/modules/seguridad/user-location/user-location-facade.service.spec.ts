import { TestBed } from '@angular/core/testing';

import { UserLocationFacadeService } from './user-location-facade.service';

describe('UserLocationFacadeService', () => {
  let service: UserLocationFacadeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(UserLocationFacadeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
