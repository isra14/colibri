import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, EMPTY, Observable } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { DataApi } from 'src/app/Model/dataApi';
import { usuarioLocationInterface } from 'src/app/Model/Seguridad/permisos.Interface';
import { usuarioInterface } from 'src/app/Model/Seguridad/usuarios.Interface';
import { DataApiService } from 'src/app/services/data-api.service';
import { ToastrServiceLocal } from 'src/app/services/toast/toastr.service';
import { MensajesHttpService } from 'src/app/utils/mensajesHttp/mensajes-http.service';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserLocationFacadeService {

   private Usuarios$ = new BehaviorSubject<usuarioInterface>({});
  public responseUsuarios$: Observable<usuarioInterface> = this.Usuarios$.asObservable();

  private UsuarioAsignado$ = new BehaviorSubject<boolean>(false);
  public responseUsuarioAsignado$: Observable<boolean> = this.UsuarioAsignado$.asObservable();

  private Ubicaciones$ = new BehaviorSubject<any[]>([]);
  public responseUbicaciones$: Observable<any[]> = this.Ubicaciones$.asObservable();

  private UsuarioUbicacion$ = new BehaviorSubject<usuarioLocationInterface[]>([]);
  public responseUsuarioUbicacion$: Observable<usuarioLocationInterface[]> = this.UsuarioUbicacion$.asObservable();

  private Cargando$ = new BehaviorSubject<boolean>(false);
  public responseCargando$: Observable<boolean> = this.Cargando$.asObservable();

  private Action$ = new BehaviorSubject<any>({});
  public responseAction$: Observable<any> = this.Action$.asObservable();

  constructor(public _toast: ToastrServiceLocal, public _dataApi: DataApiService,
    public _mensajesHttp: MensajesHttpService) { };

  setearUsuario(usuario) {
    this.Usuarios$.next(usuario);
    this.UsuarioAsignado$.next(true);
    this.mostrarUsuarioUbicacion(`${usuario.Usuario}`);
  }

  resetearUsuario() {
    this.Usuarios$.next({});
    this.UsuarioAsignado$.next(false);

  }


  mostrarUsuarioUbicacion(params?: any) {
    this.UsuarioUbicacion$.next([]);
    this.Cargando$.next(true);
    const request$ = this._dataApi.GetDataApi(`${environment.site}/api/sec/${environment.processEnv}/v1/user-ubication/`,).pipe(
      tap((result: DataApi | any) => {
        if (result.hasError === false) {
          this.UsuarioUbicacion$.next(result.data.table0);
        }
        this.Cargando$.next(false);
      }),
      catchError((error: HttpErrorResponse) => {
        this._mensajesHttp.mostrarErrorHttp(error, 'Ocurrio un error al mostrar las ubicaciones del usuario', '');
        this.Cargando$.next(false);
        return EMPTY;
      })
    );
    return request$.subscribe();
  }

  mostrarUbicaciones(params?: any) {
    this.Ubicaciones$.next([]);
    this.Cargando$.next(true);
    const request$ = this._dataApi.GetDataApi(`${environment.site}/api/admin/${environment.processEnv}/v1/admin/ubicacionesList/1`,).pipe(
      tap((result: DataApi | any) => {
        if (result.hasError === false) {
          this.Ubicaciones$.next(result.data.table0);
        }
        this.Cargando$.next(false);
      }),
      catchError((error: HttpErrorResponse) => {
        this._mensajesHttp.mostrarErrorHttp(error, 'Ocurrio un error al mostrar las ubicaciones', '');
        this.Cargando$.next(false);
        return EMPTY;
      })
    );
    return request$.subscribe();
  }

  insertarUbicacion(params: any) {
    params.evento.usuario = this.Usuarios$.value.Usuario;
    this.Cargando$.next(true);
    this._toast.mensajeLoading('Cargando', 'Procesando la información');

    const request$ = this._dataApi.PutDataApi(`${environment.site}/api/sec/${environment.processEnv}/v1/user-ubication/`, params).pipe(
      tap((result: DataApi | any) => {
        if (result.hasError === false) {
          this._toast.mensajeSuccess(`${result.data.table0[0].descripcion}`, '')
          this.agregarUbicacionUsuario(result.data.table1[0]);
        }
        this._toast.clearToasts();
        this.Cargando$.next(false);
      }),
      catchError((error: HttpErrorResponse) => {
        this._toast.clearToasts();
        this._mensajesHttp.mostrarErrorHttp(error, 'Ocurrio un error al agregar el permiso al usuario', '');
        this.Cargando$.next(false);
        return EMPTY;
      })
    );
    return request$.subscribe();
  }

  eliminarUbicacion(params: any) {
    this.Cargando$.next(true);
    this._toast.mensajeLoading('Cargando', 'Procesando la información');

    const request$ = this._dataApi.DeleteDataApi(`${environment.site}/api/sec/${environment.processEnv}/v1/user-ubication/`, params).pipe(
      tap((result: DataApi | any) => {
        if (result.hasError === false) {
          this._toast.mensajeSuccess('Se elimino la ubicacion del usuario con exito', '');
          this.mostrarUsuarioUbicacion(`${this.Usuarios$.value.Usuario}`);
        }
        this._toast.clearToasts();
        this.Cargando$.next(false);
      }),
      catchError((error: HttpErrorResponse) => {
        this._toast.clearToasts();
        this._mensajesHttp.mostrarErrorHttp(error, 'Ocurrio un error al agregar el permiso al usuario', '');
        this.Cargando$.next(false);
        return EMPTY;
      })
    );
    return request$.subscribe();
  }

  agregarUbicacionUsuario(item) {
    let ubicacionesUsuario: any[] = [];
    ubicacionesUsuario = this.UsuarioUbicacion$.value;
    ubicacionesUsuario.push(item);
    this.UsuarioUbicacion$.next([]);
    this.UsuarioUbicacion$.next(ubicacionesUsuario);
  }

}
