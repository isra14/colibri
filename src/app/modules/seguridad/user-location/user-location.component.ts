import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup } from '@angular/forms';
import { Subscription } from 'rxjs';
import { usuarioLocationInterface } from 'src/app/Model/Seguridad/permisos.Interface';
import { UserLocationFacadeService } from './user-location-facade.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-user-location',
  templateUrl: './user-location.component.html',
  styleUrls: ['./user-location.component.scss']
})
export class UserLocationComponent implements OnInit, OnDestroy {
  public filter = new FormControl('');
  public ubicacionesUsuario = new FormArray([]);
  public suscripciones: Subscription = new Subscription();
  
  constructor(public userLocationFacade: UserLocationFacadeService) {
    this.userLocationFacade.mostrarUbicaciones('');

    this.suscripciones.add(
      this.userLocationFacade.responseUbicaciones$.subscribe((result) => {
        if (result.length > 0) {
          this.setearUbicaciones(result);
        }
      })
    );

    this.suscripciones.add(
      this.userLocationFacade.responseUsuarioUbicacion$.subscribe((result: any[]) => {
        this.reiniciarValor();
        if (result.length > 0) {
          this.cambiarEstadoPermisos(result);
        }
      })
    );
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    this.suscripciones.unsubscribe();
    this.userLocationFacade.resetearUsuario();

  }

  setearUbicaciones(ubicacionesArray: any[]) {
    ubicacionesArray.forEach((p) => {
      let ubicaciones = new FormGroup({
        id: new FormControl(0),
        idUbicacion: new FormControl(p.idUbicacion),
        ubicacion: new FormControl(p.ubicacion),
        ciudad: new FormControl(p.ciudad),
        asignacion: new FormControl(false),
      });

      this.ubicacionesUsuario.push(ubicaciones);
    });

  }

  cambiarEstadoPermisos(ubicacionesUsuario: usuarioLocationInterface[]) {
    ubicacionesUsuario.forEach((u) => {
      this.ubicacionesUsuario.controls.forEach((p) => {
        if (u.idUbicacion === p.value.idUbicacion) {
          p.get('id').setValue(u.id);
          p.get('asignacion').setValue(true);
        }
      });
    });
  }

  asignar(item) {
    if (item.value.asignacion === false) {
      this.swalConfirmacion(1, item.value);
    } else {
      this.swalConfirmacion(2, item.value);
    }
  }

  swalConfirmacion(idAccion, item) {
    Swal.fire({
      title: 'Confirmación',
      html: ` <p> ¿Esta seguro quiere ${(idAccion === 1) ? 'Agregar' : 'quitar'} la ubicación: <b>${item.ubicacion}</b>? </p>`,
      icon: 'question',
      showCancelButton: true,
      confirmButtonColor: '#003399',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Confirmar',
      cancelButtonText: 'Cancelar'
    }).then((result) => {
      if (result.isConfirmed) {
        if (idAccion === 1) {
          this.userLocationFacade.insertarUbicacion({
            evento: {
              accion: "nuevo",
              idAccion: "1",
              usuario: "",
              idUbicacion: item.idUbicacion
            }
          });
        }
        if (idAccion === 2) {
          this.userLocationFacade.eliminarUbicacion(`${item.id}`);
        }


      } else {
        this.ubicacionesUsuario.controls.forEach((p) => {
          if (p.get('idUbicacion').value === item.idUbicacion) {
            p.get('asignacion').setValue(item.asignacion);
          }
        });
      }
    });
  }

  reiniciarValor() {
    this.ubicacionesUsuario.controls.forEach((p) => {
      p.get('asignacion').setValue(false);
    });
  }

}
