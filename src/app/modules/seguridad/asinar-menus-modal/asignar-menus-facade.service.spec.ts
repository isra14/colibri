import { TestBed } from '@angular/core/testing';

import { AsignarMenusFacadeService } from './asignar-menus-facade.service';

describe('AsignarMenusFacadeService', () => {
  let service: AsignarMenusFacadeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AsignarMenusFacadeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
