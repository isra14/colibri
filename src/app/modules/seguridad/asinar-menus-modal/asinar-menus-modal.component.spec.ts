import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AsinarMenusModalComponent } from './asinar-menus-modal.component';

describe('AsinarMenusModalComponent', () => {
  let component: AsinarMenusModalComponent;
  let fixture: ComponentFixture<AsinarMenusModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AsinarMenusModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AsinarMenusModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
