import { Injectable } from '@angular/core';
import { FormControl } from '@angular/forms';
import { format, parseISO } from 'date-fns';

@Injectable({
  providedIn: 'root'
})
export class ValidateReactiveFormService {

  constructor() { }

  //Validacion permite solo un espacio pero sin evaluar caracteres
  public validacionUnEspacio(control: FormControl) {
    if (control.value === null || control.value == '') {
      return;
    }

    if (control.value.charAt(0) === " ") {
      control.setValue("");
    }

    for (let i = 0; i < control.value.length; i++) {

      if (control.value.length > 0 && control.value.charAt(i) === " ") {
        if (control.value.charAt(i + 1) === " ") {
          control.setValue(control.value.slice(0, control.value.length - 1));
        }
      }
    }
    return null;
  }

  //Validacion de inputs sin caracteres ni numeros
  public validacionUnEspacioSinCaracteresNumeros(control: FormControl) {
    if (control.value === null || control.value == '') {
      return;
    }
    // let txtCadena= new String(cadena);
    let caracteres = `!"#$%&'()*+,-./[\]^_{|}~@123456789`;

    if (control.value.charAt(0) === " ") {
      control.setValue("");
    }

    for (let i = 0; i < control.value.length; i++) {
      if (caracteres.indexOf(control.value.charAt(i), 0) != -1) {
        control.setValue(control.value.slice(0, control.value.length - 1));
      }
      if (control.value.length > 0 && control.value.charAt(i) === " ") {
        if (control.value.charAt(i + 1) === " ") {
          control.setValue(control.value.slice(0, control.value.length - 1));
        }
      }
    }
    return null;
  }

  //Validacion del input sin caracteres especiales pero permitiendo numeros
  public validacionUnEspacioSinCaracteres(control: FormControl) {
    if (control.value === null || control.value == '') {
      return;
    }
    // let txtCadena= new String(cadena);
    let caracteres = `!"#$%&'()*+,-./[\]^_{|}~@;`;

    if (control.value.charAt(0) === " ") {
      control.setValue("");
    }

    for (let i = 0; i < control.value.length; i++) {
      if (caracteres.indexOf(control.value.charAt(i), 0) != -1) {
        control.setValue(control.value.slice(0, control.value.length - 1));
      }
      if (control.value.length > 0 && control.value.charAt(i) === " ") {
        if (control.value.charAt(i + 1) === " ") {
          control.setValue(control.value.slice(0, control.value.length - 1));
        }
      }
    }
    return null;
  }


  //Validacion para evitar caracteres que el programador especifique.
  public validacionPersonalizada(control: FormControl, caracteres: string) {
    if (control.value === null || control.value == '') {
      return;
    }

    if (control.value.charAt(0) === " ") {
      control.setValue("");
    }

    for (let i = 0; i < control.value.length; i++) {
      if (caracteres.indexOf(control.value.charAt(i), 0) != -1) {
        control.setValue(control.value.slice(0, control.value.length - 1));
      }
      if (control.value.length > 0 && control.value.charAt(i) === " ") {
        if (control.value.charAt(i + 1) === " ") {
          control.setValue(control.value.slice(0, control.value.length - 1));
        }
      }
    }
    return null;
  }

  //Validacion sin espacios
  validacionSinEspacios(control: FormControl) {

    if (control.value.charAt(0) === " ") {
      control.setValue("");
    }

    for (let i = 0; i < control.value.length; i++) {

      if (control.value.length > 0 && control.value.charAt(i) === " ") {

        control.setValue(control.value.slice(0, control.value.length - 1));

      }

    }
    return null;

  }

  //Validacion sin espacios, sin caracteres especiales y sin numeros
  validacionSinEspaciosCaracteresNumeros(control: FormControl) {

    if (control.value.charAt(0) === " ") {
      control.setValue("");
    }

    let caracteres = `!"#$%&'()*+,-./[\]^_{|}~@123456789`;

    for (let i = 0; i < control.value.length; i++) {

      if (caracteres.indexOf(control.value.charAt(i), 0) != -1) {
        control.setValue(control.value.slice(0, control.value.length - 1));
      }

      if (control.value.length > 0 && control.value.charAt(i) === " ") {

        control.setValue(control.value.slice(0, control.value.length - 1));

      }

    }
    return null;

  }

  //Validacion sin espacios, sin caracteres especiales y sin numeros
  validacionSinEspaciosCaracteres(control: FormControl) {

    if (control.value.charAt(0) === " ") {
      control.setValue("");
    }

    let caracteres = `!"#$%&'()*+,-./[\]^_{|}~@`;


    for (let i = 0; i < control.value.length; i++) {

      if (caracteres.indexOf(control.value.charAt(i), 0) != -1) {
        control.setValue(control.value.slice(0, control.value.length - 1));
      }

      if (control.value.length > 0 && control.value.charAt(i) === " ") {

        control.setValue(control.value.slice(0, control.value.length - 1));

      }

    }
    return null;

  }

  public validacionEspacios(control: FormControl) {
    if (control.value == null) {
      return;
    }
    // let txtCadena= new String(cadena);
    let caracteres = `!"#$%&'()*+,-./[\]^_{|}~@`;

    if (control.value.charAt(0) === " ") {
      control.setValue("");
    }

    for (let i = 0; i < control.value.length; i++) {
      if (caracteres.indexOf(control.value.charAt(i), 0) != -1) {
        control.setValue(control.value.slice(0, control.value.length - 1));
      }
      if (control.value.length > 0 && control.value.charAt(i) === " ") {
        if (control.value.charAt(i + 1) === " ") {
          control.setValue(control.value.slice(0, control.value.length - 1));
        }
      }
    }
    return null;
  }

  //Validacion sin espacios, sin caracteres especiales y sin numeros
  validacionSinEspaciosCaracteresPersonalizados(control: FormControl, caracteres: string) {

    if (control.value.charAt(0) === " ") {
      control.setValue("");
    }

    for (let i = 0; i < control.value.length; i++) {

      if (caracteres.indexOf(control.value.charAt(i), 0) != -1) {
        control.setValue(control.value.slice(0, control.value.length - 1));
      }

      if (control.value.length > 0 && control.value.charAt(i) === " ") {

        control.setValue(control.value.slice(0, control.value.length - 1));

      }

    }
    return null;

  }

 
  
  
}
