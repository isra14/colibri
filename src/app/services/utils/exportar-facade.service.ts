import { HttpErrorResponse } from '@angular/common/http';
import { ElementRef, Injectable, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, EMPTY, Observable } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { DataApi } from 'src/app/Model/dataApi';
import { MensajesHttpService } from 'src/app/utils/mensajesHttp/mensajes-http.service';
import { environment } from 'src/environments/environment';
import { DataApiService } from '../data-api.service';
import { ToastrServiceLocal } from '../toast/toastr.service';
import * as XLSX from 'xlsx';


//Exportar a excel
import { Workbook } from 'exceljs';
import * as fs from 'file-saver';
import { DatePipe } from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class ExportarFacadeService {
  
  constructor(public _toast: ToastrServiceLocal, public _dataApi: DataApiService,
    public _mensajesHttp: MensajesHttpService, private router: Router, private datePipe: DatePipe) { };

  private Cargando$ = new BehaviorSubject<boolean>(false);
  public responseCargando$: Observable<boolean> = this.Cargando$.asObservable();
  public isExcelFile: boolean;
  private keys: string[];

  //Excel
  public ExcelName = '';
  private Excel$ = new BehaviorSubject<any>([]);
  public responseExcel: Observable<any[]> = this.Excel$.asObservable();
   
  recorrerObjetos(data, items: any[], name: string) {

    let columnas: string[] = [];
    let body: any[] = [];

    //Columnas
    for (const i in data) {
      columnas.push(data[i].columna);
    }

    //Recorrer la data y crear un arreglo de objetos apartir de ella usando como key las columnas antes especificadas
    for (let i = 0; i < items.length; i++) {
      let bodyObject = {}

      data.forEach((res) => {
        if (String(items[i][res.value]) === 'null') {
          bodyObject[res.columna.replace(/ /g, "")] = '-';
        } else {
  
          bodyObject[res.columna.replace(/ /g, "")] = String(items[i][res.value]);
        }
      });

      body.push(bodyObject);
    }
    let date = new Date();

    let nombreArchivo = String(`${name}-0${date.getDate()}-0${date.getMonth() + 1}-${date.getFullYear()}-${date.getHours()}:${date.getMinutes()}:${date.getSeconds()}`);

    this.exportarExcel({
      nameFile: `${nombreArchivo}.xlsx`,
      Worksheet: "info",
      columns: columnas,
      body: body
    });

  }

  exportarExcel(params?: any) {
    this.Cargando$.next(true);
    this._toast.mensajeLoading('Cargando', 'Procesando la información');

    const request$ = this._dataApi.PostDataApi(`${environment.site}/api/utils/${environment.processEnv}/v1/exportExcel`, params, 4).pipe(
      tap(({ result }: DataApi | any) => {
        this._toast.clearToasts();
        this.Cargando$.next(false);
        if (result.hasError === false) {
          this._toast.clearToasts();
          this.manageExcelFile(result.data[0].url);
          this._toast.mensajeSuccess('Se exporto con exito el archivo de excel', '');
        }
      }),
      catchError((error: HttpErrorResponse) => {
        this._toast.clearToasts();
        this._mensajesHttp.mostrarErrorHttp(error, 'Ocurrio un error al generar el archivo excel', '');
        this.Cargando$.next(false);
        return EMPTY;
      })
    );
    return request$.subscribe();
  }

  manageExcelFile(url) {
    window.open(url);
  }

  exportExcel(titulo, header, table: any[], fname) {
    let title = '';

    if (titulo != null) {
      title = this.titulo(titulo);
    }
 
    let workbook = new Workbook();
    let worksheet = workbook.addWorksheet(`${fname}`);

    // const logo = workbook.addImage({
    //   base64: this.base64,
    //   extension: 'png',
    // });

    // worksheet.addImage(logo, {
    //   tl: { col: 0, row: 0 },
    //   ext: { width: 148, height: 38 }
    // });

    const fecha = worksheet.getCell('A8');
    fecha.value = ('Fecha : ' + this.datePipe.transform(new Date(), 'yyyy-MM-dd'));
    fecha.font = {
      name: "Arial",
      family: 4,
      size: 11,
      bold: true
    };
    let dataUsuario = JSON.parse(localStorage.getItem(`${environment.dataUsuarioLocal}`));
    const usuario = worksheet.getCell('A9');
    usuario.value = ('Usuario : ' + dataUsuario.Usuario);
    usuario.font = {
      name: "Arial",
      family: 4,
      size: 11,
      bold: true
    };

    let filaInicio = 10;
    
    for (const i in table) {
      let data = this.exportFromTable(table[i]);

      let columns: any[] = [];
      let rows: any[] = [];



      for (const i in header) {
        columns.push({ name: header[i], filterButton: true },)
      }

      for (let x1 of data) {
        let x2 = Object.keys(x1);
        let temp = []
        for (let y of x2) {
          temp.push(x1[y])
        }
        rows.push(temp)
      }
      
      worksheet.addTable({
        name: `MyTable${filaInicio}`,
        ref: `A${filaInicio}`,
        headerRow: true,
        style: {
          theme: 'TableStyleMedium13',
          showRowStripes: true,

        },
        columns: columns,
        rows: rows
      });
      
      filaInicio = filaInicio + rows.length + 3;
    }
    


    worksheet.columns.forEach(function (column, i) {
      var maxLength = 0;
      column["eachCell"]({ includeEmpty: true }, function (cell) {

        var columnLength = cell.value ? cell.value?.toString().length : 10;
        if (columnLength > maxLength) {
          maxLength = columnLength;
        }
      });
      column.font = {}
      column.alignment = { vertical: 'middle', horizontal: 'center' };
      column.width = maxLength < 10 ? 5 : maxLength + 5;
    });

    const customCell = worksheet.getCell("A1");
    customCell.font = {
      name: "Arial",
      family: 4,
      size: 11,
      bold: true
    };
    customCell.alignment = { vertical: 'middle', horizontal: 'center', wrapText: true };

    customCell.value = title != null ? title : ' ';

    worksheet.mergeCells("A1", 'F7');

    // add data and file name and download
    workbook.xlsx.writeBuffer().then((data) => {
      let blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
      fs.saveAs(blob, fname + '-' + new Date().valueOf() + '.xlsx');
    });

  }

  public exportFromTable(table){
    let csv = [],
      tableRows = table.querySelectorAll("tr")

    for (let i = 0; i < tableRows.length; i++) {
      let row = [],
        cols = tableRows[i].querySelectorAll("td, th")

      for (let j = 0; j < cols.length; j++) {
        let col = cols[j].innerHTML;
        col = col.replace(/(<([^>]+)>)/ig, "");
        if(col != ""){
          row.push(col);
        }
      }
      
      csv.push(row)
    }
    return csv;
  }

  public titulo(titulo) {
    let parserTitulo = titulo.innerHTML
    parserTitulo = parserTitulo.replace(/<br[^>]*>/gi, "\n");
    parserTitulo = parserTitulo.replace(/<span[^>]*>/gi, "\n");
    parserTitulo = parserTitulo.replace(/<[/>]span[^>]*>/gi, "\n");
    return parserTitulo;
  }
  @ViewChild('inputFile') inputFile: ElementRef;

  onChange(evt) {

    let data;

    const target: DataTransfer = <DataTransfer>(evt.target);
    this.ExcelName = target.files[0].name;
    this.isExcelFile = !!target.files[0].name.match(/(.xls|.xlsx)/);
    console.log(this.ExcelName)
    if (target.files.length > 1) {
      this.inputFile.nativeElement.value = '';
    }

    if (this.isExcelFile) {

      const reader: FileReader = new FileReader();

      reader.onload = (e: any) => {

        //leer excel
        const bstr: string = e.target.result;
        const wb: XLSX.WorkBook = XLSX.read(bstr, { type: 'binary' });

        //lee la primera hoja
        const wsname: string = wb.SheetNames[0];
        const ws: XLSX.WorkSheet = wb.Sheets[wsname];

        //guardar datos
        data = XLSX.utils.sheet_to_json(ws);

      };

      reader.readAsBinaryString(target.files[0]);

      reader.onloadend = (e) => {

        this.keys = Object.keys(data[0]);
        this.Excel$.next(data);

      }
    } else {
      this.inputFile.nativeElement.value = '';
    }
  }



}
