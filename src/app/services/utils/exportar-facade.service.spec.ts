import { TestBed } from '@angular/core/testing';

import { ExportarFacadeService } from './exportar-facade.service';

describe('ExportarFacadeService', () => {
  let service: ExportarFacadeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ExportarFacadeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
