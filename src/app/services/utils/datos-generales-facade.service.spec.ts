import { TestBed } from '@angular/core/testing';

import { DatosGeneralesFacadeService } from './datos-generales-facade.service';

describe('DatosGeneralesFacadeService', () => {
  let service: DatosGeneralesFacadeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DatosGeneralesFacadeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
