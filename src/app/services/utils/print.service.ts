import { DatePipe } from '@angular/common';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PrintService {
  public dataUsuario:any;
  public fecha:string = "";

  constructor(private datePipe: DatePipe) {
    // this.dataUsuario = JSON.parse(localStorage.getItem(environment.dataUsuarioLocal));
    this.fecha = this.datePipe.transform(new Date(), 'yyyy-MM-dd');
  }
  
  public pagina = `@page {
    display: block !important;
    margin: 0 auto;
    margin-top: 1cm;
    margin-bottom: 0.5cm;
    box-shadow: 0 0 0.5cm rgba(0, 0, 0, 0.5);
    padding: 1cm !important;
    -webkit-print-color-adjust: exact;
  }`;

  public estilosImprimir = `
  @media print {
    .show {
      display: initial !important;
    }
    .page {
      background-repeat: space !important;
      background-position: initial !important;
      margin: 0 !important;
      padding: 0 !important;
      background-size: 1050px 1350px !important;
      width: 1050px !important;
      max-height: 1350px !important;
      min-height: 1350px !important;
    }

    .DivHeader {
      position: fixed !important;
      top: 0 !important;
    }

    /*stylos propios*/
    .hidden {
      display: none !important;
    }

    .page-break-after {
      display: block;
      page-break-after: always !important;
    }

    .page-break-before {
      display: block;
      page-break-before: always !important;
    }

    .header {
      text-align: left !important;
      font-family: Cambria !important;
      font-size: 10pt !important;
      max-width: 20cm !important;
      max-height: 20cm !important;
      font-style: italic;
      font-weight: bold;
      padding-right: 4cm;
      padding-left: 4cm;
      padding-top: 5cm;
      padding-bottom: 0cm;
    }
    .header-paginacion {
      text-align: right !important;
      font-family: Cambria !important;
      font-size: 12pt !important;
      font-weight: bold;
      margin: 0;
      padding-right: 3cm !important;
      padding-left: 0;
      padding-top: 0;
      padding-bottom: 0cm;
      z-index: -1;
    }
    .contenido-normal-justificado {
      text-align: justify;
      font-family: "Arial Narrow", sans-serif;
      font-size: 18pt;
      padding: 0.5cm 4cm;
      line-height: 1cm;
    }

    .contenido-normal-centrado {
      text-align: center;
      font-weight: bold;
      font-family: Arial Narrow;
      font-size: 18pt;
      padding: 0.1cm 4cm;
      line-height: 1cm;
    }
    .sub-contenido-normal-centrado {
      margin-top: 2cm !important;
      margin-bottom: 0 !important;
      text-align: center !important;
      font-family: Arial Narrow;
      font-size: 16pt;
      font-style: italic;
    }

    .contenido-tabla {
      margin-top: 0.5cm !important;
      text-align: center !important;
      font-family: Arial Narrow !important;
      font-size: 16pt !important;
    }

    .titulo-1 {
      font-weight: bold;
      text-align: left;
      font-family: Arial Narrow;
      font-size: 15pt;
      text-transform: uppercase;
      padding: 0cm 4cm;
    }

    .titulo-2 {
      margin-top: 0.2cm;
      padding: 0;
      font-weight: bold;
      text-align: left;
      font-family: Arial Narrow;
      font-size: 15pt;
      padding: 0cm 4cm;
    }
    /*fin stylos propios*/
    *,
    *::before,
    *::after {
      text-shadow: none !important;
      box-shadow: none !important;
    }
    a,
    a:visited {
      text-decoration: underline;
    }
    abbr[title]::after {
      content: " (" attr(title) ")";
    }
    pre {
      white-space: pre-wrap !important;
    }
    pre,
    blockquote {
      border: 1px solid #999;
      page-break-inside: avoid;
    }
    thead {
      display: table-header-group;
    }
    tr,
    img {
      page-break-inside: avoid;
    }
    p,
    h2,
    h3 {
      orphans: 3;
      widows: 3;
    }
    h2,
    h3 {
      page-break-after: avoid;
    }
    .navbar {
      display: none;
    }
    .badge {
      border: 1px solid #000;
    }

    .tabla {
      border-collapse: collapse !important;
      border: 1px solid !important;
      border-radius: 10px !important;
      padding: 0.5cm 3cm !important;
      margin: 0.5cm 1cm !important;
      text-align: center;
    }
    .tabla thead {
      background-color: #fff !important;
      border: 1px solid #000 !important;
    }

    .tabla td {
      background-color: #fff !important;
      border: 1px solid #000 !important;
      padding: 8px !important;
      margin-top: 0 !important;
      margin-bottom: 0 !important;
      margin-right: 0.5px !important;
      margin-left: 0.5px !important;
      font-size: 10pt !important;
    }
    .tabla th {
      border: 1px solid #000 !important;
      padding: 8px !important;
      margin-top: 0 !important;
      margin-bottom: 0 !important;
      margin-right: 0.5px !important;
      margin-left: 0.5px !important;
    }
    .tabla-bordered th,
    .tabla-bordered td {
      border: 1px solid #ddd !important;
      border: 1px solid !important;
      border-radius: 10px !important;
    }
    .tableRow {
      display: table;
      width: 100%; /*Optional*/
      table-layout: fixed; /*Optional*/
      border-spacing: 10px; /*Optional*/
    }
    .Column {
      display: table-cell;
    }
    .footer {
      position: fixed;
      bottom: 0;
    }
    .divImagePrint{
      width: 180px !important; 
      height: 70px !important;
    }
    
  }
  `;

  imprimirElemento(elemento: any, hojasEstilo?: any, orientacion?:number) {
    let style:String = "";

    if(orientacion === 1){
      this.pagina = `
      @page body{
        display: block !important;
        margin: 0 auto;
        margin-top: 1cm;
        margin-bottom: 0.5cm;
        box-shadow: 0 0 0.5cm rgba(0, 0, 0, 0.5);
        padding: 1cm !important;
        -webkit-print-color-adjust: exact;
      }`
    }; 
    if(orientacion === 2){
      this.pagina = `
      @page {
        display: block !important;
        size: landscape;
        margin: 0 auto;
        margin-top: 1cm;
        margin-bottom: 0.5cm;
        box-shadow: 0 0 0.5cm rgba(0, 0, 0, 0.5);
        padding: 1cm !important;
        -webkit-print-color-adjust: exact;
      }`
    };

    style = this.estilosImprimir + this.pagina;

    //var ventana = window.open('','PRINT','height=600,width=800');
    var ventana = window.open(null, 'PRINT', 'fullscreen=1,location=0,titlebar=0,menubar=0,toolbar=0,status=0');
    // ventana.document.write('<html><head><title>' + document.title + '</title>');
    ventana.document.write(`<html><head><title></title>`);
    ventana.document.write('<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">');


    if (hojasEstilo.length > 0) {
      for (let estilo of hojasEstilo) {
        ventana.document.write(`<style type="text/css">
              ${estilo.estilo} ${style}
              </style>`);
      }
    }
    // ventana.document.write(` 
    //   <div class="text-right"> 
    //     <span style="font-size: 10pt;">Fecha Impresion: ${this.fecha}</span> 
    //     <br>
    //     <span style="font-size: 10pt;">Usuario: ${this.dataUsuario.Usuario} </span>
    //   </div>`
    // );
    ventana.document.write('</head><body>');
    ventana.document.write(elemento.innerHTML);
    ventana.document.write('</body></html>');
    ventana.document.close();
    ventana.focus();
    ventana.onload = function () {
      ventana.print();
      // ventana.close();
    };
    return true;


  }


  tableToExcel(elemento: any) {

    var tab_text = "<table border='2px'><tr bgcolor='#87AFC6'>";
    var textRange; var j = 0;
    let tab = elemento; // id of table
    let txtArea1; // id of table
    let sa;
    

    let base64 = function (s) {
      return window.btoa(unescape(encodeURIComponent(s)))
    };

    for (j = 0; j < tab.rows.length; j++) {
      tab_text = tab_text + tab.rows[j].innerHTML + "</tr>";
      //tab_text=tab_text+"</tr>";
    }

    tab_text = tab_text + "</table>";
    tab_text = tab_text.replace(/<A[^>]*>|<\/A>/g, "");//remove if u want links in your table
    tab_text = tab_text.replace(/<img[^>]*>/gi, ""); // remove if u want images in your table
    tab_text = tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); // reomves input params

    var ua = window.navigator.userAgent;
    var msie = ua.indexOf("MSIE ");

    if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer
    {
      txtArea1.document.open("txt/html", "replace");
      txtArea1.document.write(tab_text);
      txtArea1.document.close();
      txtArea1.focus();
      sa = txtArea1.document.execCommand("SaveAs", true, "Say Thanks to Sumit.xlsx");
    }
    else {
      sa = window.open('data:application/vnd.ms-excel,' + encodeURIComponent(tab_text));
    }
   
    
    return false;
  }

  exportTableToExcel(elemento, filename) {
    let extension = '.XLS';

    let base64 = function (s) {
      return window.btoa(unescape(encodeURIComponent(s)))
    };

    let template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>';
    let render = function (template, content) {

      return template.replace(/{(\w+)}/g, function (m, p) { return content[p]; });
    };


    let tableElement = elemento;


    let tableExcel = render(template, {
      worksheet: filename,
      table: tableElement.innerHTML
    });

    filename = filename + extension;


  
  }

}
