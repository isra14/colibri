import { TestBed } from '@angular/core/testing';

import { ModalPuntoVentaService } from './modal-punto-venta-service.service';

describe('ModalPuntoVentaServiceService', () => {
  let service: ModalPuntoVentaService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ModalPuntoVentaService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
