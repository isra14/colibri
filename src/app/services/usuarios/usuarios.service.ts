import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { EMPTY } from 'rxjs';
import { BehaviorSubject, Observable } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { DataApi } from '../../Model/dataApi';
import { MensajesHttpService } from '../../utils/mensajesHttp/mensajes-http.service';
import { DataApiService } from '../data-api.service';
import { ToastrServiceLocal } from '../toast/toastr.service';

@Injectable({
  providedIn: 'root'
})
export class UsuariosService {
public loadingUsers:boolean
  constructor( public router: Router, public _dataApi: DataApiService, public _toast: ToastrServiceLocal, 
    public _mensajesHttp: MensajesHttpService) {
     }

  getUsuarios(params: any, callback:any) {
    this.loadingUsers=true
    let url = `${environment.site}/api/consulta/${environment.processEnv}/v1/usuarios`;
    const request$ = this._dataApi.GetDataApi(url).pipe(
      tap((result: DataApi| any) => {
        if (!result.hasError) {
          this.loadingUsers=false
          callback(result);
        } else {
          result.errors.forEach(element => {
            this._toast.mensajeInfo(element.descripcion, '');
      this.loadingUsers=false


          });
          callback();
        }
      }),
      catchError((error: HttpErrorResponse) => {
        this._mensajesHttp.mostrarErrorHttp(error, 'Ocurrio un error', '');
        return EMPTY;
      })
    );
    return request$.subscribe();
  }

  changeRol(params: any, callback:any) {
    this._toast.mensajeLoading('Cargando', 'Procesando la información');
    let url = `${environment.site}/api/consulta/${environment.processEnv}/v1/usuarios/${params.id}/rol`;
    const request$ = this._dataApi.PutDataApi(url,params).pipe(
      tap((result: DataApi| any) => {
        this._toast.clearToasts();
        if (!result.hasError) {
          
          callback(result);
        } else {
          result.errors.forEach(element => {

            this._toast.mensajeInfo(element.descripcion, '');

          });
          callback();
        }
      }),
      catchError((error: HttpErrorResponse) => {
        this._toast.clearToasts();
        this._mensajesHttp.mostrarErrorHttp(error, 'Ocurrio un error', '');
        return EMPTY;
      })
    );
    return request$.subscribe();
  }
}
