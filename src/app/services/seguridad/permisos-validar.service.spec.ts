import { TestBed } from '@angular/core/testing';

import { PermisosValidarService } from './permisos-validar.service';

describe('PermisosValidarService', () => {
  let service: PermisosValidarService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PermisosValidarService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
