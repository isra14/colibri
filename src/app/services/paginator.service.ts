import { Inject, Injectable, OnDestroy, Optional } from '@angular/core';
import { PageEvent } from '@angular/material/paginator';

@Injectable({
  providedIn: 'root'
})
export class PaginatorService  {
  
  //Paginacion

  public pageSize = 10;
  public page = 0;
  public pageEvent: PageEvent;
  public pageIndex: number = 0;
  public desde = 0;
  public hasta = 10;
  
  constructor( ) {
    
  }


  //Paginación de la tabla
  next(event: PageEvent) {

    if (event.pageIndex === this.pageIndex + 1) {
      this.desde = this.desde + this.pageSize;
      this.hasta = this.hasta + this.pageSize;
    }
    else if (event.pageIndex === this.pageIndex - 1) {
      this.desde = this.desde - this.pageSize;
      this.hasta = this.hasta - this.pageSize;
    }
    this.pageIndex = event.pageIndex;
  }

  reiniciarVariables() {
    this.desde = 0;
    this.hasta = 10;
    this.pageIndex = 0;
  }
}
