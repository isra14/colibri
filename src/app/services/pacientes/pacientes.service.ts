import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { EMPTY } from 'rxjs';
import { BehaviorSubject, Observable } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { DataApi } from '../../Model/dataApi';
import { MensajesHttpService } from '../../utils/mensajesHttp/mensajes-http.service';
import { DataApiService } from '../data-api.service';
import { ToastrServiceLocal } from '../toast/toastr.service';

@Injectable({
  providedIn: 'root'
})
export class PacientesService {
  public loading:boolean
  public loadingUno:boolean
  public loadingEncuesta:boolean
  public loadingEncuestaDetalle:boolean=true

  private CargandoPacientes$ = new BehaviorSubject<boolean>(false);
  public responsePacientes$: Observable<boolean> = this.CargandoPacientes$.asObservable();


  constructor( public router: Router, public _dataApi: DataApiService, public _toast: ToastrServiceLocal, 
    public _mensajesHttp: MensajesHttpService) {
     }

  getPacientes(params: any, callback:any) {
    this.loading=true
    let url = `${environment.site}/api/consulta/${environment.processEnv}/v1/persona/${params}/datospersonales`;
    const request$ = this._dataApi.GetDataApi(url).pipe(
      tap((result: DataApi| any) => {
        if (!result.hasError) {
          this.loading=false
          callback(result);
        } else {
          result.errors.forEach(element => {
            this._toast.mensajeInfo(element.descripcion, '');
      this.loading=false


          });
          callback();
        }
      }),
      catchError((error: HttpErrorResponse) => {
        this._mensajesHttp.mostrarErrorHttp(error, 'Ocurrio un error', '');
        return EMPTY;
      })
    );
    return request$.subscribe();
  }

  getEncuesta(params: any, callback) {
    this.loadingEncuesta=true;
    let url = `${environment.site}/api/consulta/${environment.processEnv}/v1/consulta/${params}/0`;
    const request$ = this._dataApi.GetDataApi(url).pipe(
      tap((result: DataApi| any) => {
        this.loadingEncuesta=false;
        if (!result.hasError) {
          callback(result);
        } else {
          result.errors.forEach(element => {
            this._toast.mensajeInfo(element.descripcion, '');
               this.loadingEncuesta=false

          });
          callback();
        }
      }),
      catchError((error: HttpErrorResponse) => {
        this._mensajesHttp.mostrarErrorHttp(error, 'Ocurrio un error', '');
        this.loadingEncuesta=false

        return EMPTY;
      })
    );
    return request$.subscribe();
  }
  getEncuestaDetalle(params: any, callback) {
    this.loadingEncuestaDetalle=true;
    let url = `${environment.site}/api/consulta/${environment.processEnv}/v1/consulta/${params.id}/${params.idDetalle}/detalle`;
    const request$ = this._dataApi.GetDataApi(url).pipe(
      tap((result: DataApi| any) => {
        if (!result.hasError) {
          this.loadingEncuestaDetalle=false;
          callback(result);
        } else {
          result.errors.forEach(element => {
            this._toast.mensajeInfo(element.descripcion, '');
               this.loadingEncuestaDetalle=false

          });
          callback();
        }
      }),
      catchError((error: HttpErrorResponse) => {
        this._mensajesHttp.mostrarErrorHttp(error, 'Ocurrio un error', '');
        this.loadingEncuestaDetalle=false

        return EMPTY;
      })
    );
    return request$.subscribe();
  }


  getEtnias(params: any, callback) {
    let url = `${environment.site}/api/consulta/${environment.processEnv}/v1/grupoetnico`;
    const request$ = this._dataApi.GetDataApi(url).pipe(
      tap((result: DataApi| any) => {
        if (!result.hasError) {
          callback(result);
        } else {
          result.errors.forEach(element => {
            this._toast.mensajeInfo(element.descripcion, '');

          });
          callback();
        }
      }),
      catchError((error: HttpErrorResponse) => {
        this._mensajesHttp.mostrarErrorHttp(error, 'Ocurrio un error', '')

        return EMPTY;
      })
    );
    return request$.subscribe();
  }

  getPaciente(params: any, callback:any) {
   this.loadingUno=true
    let url = `${environment.site}/api/consulta/${environment.processEnv}/v1/persona/${params}/datospersonales`;
    const request$ = this._dataApi.GetDataApi(url).pipe(
      tap((result: DataApi| any) => {
        if (!result.hasError) {
          this.loadingUno=false
          callback(result);
        } else {
          result.errors.forEach(element => {
            this._toast.mensajeInfo(element.descripcion, '');
            this.loadingUno=false
          });
          callback();
        }
      }),
      catchError((error: HttpErrorResponse) => {
        this._mensajesHttp.mostrarErrorHttp(error, 'Ocurrio un error', '');
        return EMPTY;
      })
    );
    return request$.subscribe();
  }
  updatePerfil(params: any, callback:any) {
    // this.loading=true
    this._toast.mensajeLoading('Cargando', 'Procesando la información');
     let url = `${environment.site}/api/consulta/${environment.processEnv}/v1/persona/${params.id}`;
     const request$ = this._dataApi.PutDataApi(url,params).pipe(
       tap((result: DataApi| any) => {
        this._toast.clearToasts();
         if (!result.hasError) {
          //  this.loadingUno=false
           callback(result);
         } else {
           result.errors.forEach(element => {
             this._toast.mensajeInfo(element.descripcion, '');
            //  this.loadingUno=false
           });
           callback();
         }
       }),
       catchError((error: HttpErrorResponse) => {
        this._toast.clearToasts();
         this._mensajesHttp.mostrarErrorHttp(error, 'Ocurrio un error', '');
         return EMPTY;
       })
     );
     return request$.subscribe();
   }

   saveCita(params: any, callback:any) {
    // this.loading=true
    this._toast.mensajeLoading('Cargando', 'Procesando la información');
     let url = `${environment.site}/api/consulta/${environment.processEnv}/v1/persona/${params.idPaciente}/citaMedica`;
     const request$ = this._dataApi.PostDataApi(url,params).pipe(
       tap((result: DataApi| any) => {
        this._toast.clearToasts();
         if (!result.hasError) {
          //  this.loadingUno=false
           callback(result);
         } else {
           result.errors.forEach(element => {
             this._toast.mensajeInfo(element.descripcion, '');
            //  this.loadingUno=false
           });
           callback();
         }
       }),
       catchError((error: HttpErrorResponse) => {
        this._toast.clearToasts();
         this._mensajesHttp.mostrarErrorHttp(error, 'Ocurrio un error', '');
         return EMPTY;
       })
     );
     return request$.subscribe();
   }
   updateCita(params: any, callback:any) {
    // this.loading=true
    this._toast.mensajeLoading('Cargando', 'Procesando la información');
     let url = `${environment.site}/api/consulta/${environment.processEnv}/v1/persona/${params.idPaciente}/citaMedica/${params.idCita}`;
     const request$ = this._dataApi.PutDataApi(url,params).pipe(
       tap((result: DataApi| any) => {
        this._toast.clearToasts();
         if (!result.hasError) {
          //  this.loadingUno=false
           callback(result);
         } else {
           result.errors.forEach(element => {
             this._toast.mensajeInfo(element.descripcion, '');
            //  this.loadingUno=false
           });
           callback();
         }
       }),
       catchError((error: HttpErrorResponse) => {
        this._toast.clearToasts();
         this._mensajesHttp.mostrarErrorHttp(error, 'Ocurrio un error', '');
         return EMPTY;
       })
     );
     return request$.subscribe();
   }



   getCitasPaciente(params: any, callback:any) {
    this.loadingUno=true
     let url = `${environment.site}/api/consulta/${environment.processEnv}/v1/persona/${params}/citamedica`;
     const request$ = this._dataApi.GetDataApi(url).pipe(
       tap((result: DataApi| any) => {
         if (!result.hasError) {
           this.loadingUno=false
           callback(result);
         } else {
           result.errors.forEach(element => {
             this._toast.mensajeInfo(element.descripcion, '');
             this.loadingUno=false
           });
           callback();
         }
       }),
       catchError((error: HttpErrorResponse) => {
         this._mensajesHttp.mostrarErrorHttp(error, 'Ocurrio un error', '');
         return EMPTY;
       })
     );
     return request$.subscribe();
   }
}
