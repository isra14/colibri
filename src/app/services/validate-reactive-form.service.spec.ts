import { TestBed } from '@angular/core/testing';

import { ValidateReactiveFormService } from './validate-reactive-form.service';

describe('ValidateReactiveFormService', () => {
  let service: ValidateReactiveFormService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ValidateReactiveFormService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
