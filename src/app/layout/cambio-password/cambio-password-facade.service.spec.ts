import { TestBed } from '@angular/core/testing';

import { CambioPasswordFacadeService } from './cambio-password-facade.service';

describe('CambioPasswordFacadeService', () => {
  let service: CambioPasswordFacadeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CambioPasswordFacadeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
