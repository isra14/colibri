import { TestBed } from '@angular/core/testing';

import { MosaicMenuFacadeService } from './mosaic-menu-facade.service';

describe('MosaicMenuFacadeService', () => {
  let service: MosaicMenuFacadeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MosaicMenuFacadeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
