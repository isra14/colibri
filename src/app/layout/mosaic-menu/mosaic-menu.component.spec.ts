import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MosaicMenuComponent } from './mosaic-menu.component';

describe('MosaicMenuComponent', () => {
  let component: MosaicMenuComponent;
  let fixture: ComponentFixture<MosaicMenuComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MosaicMenuComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MosaicMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
