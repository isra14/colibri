import { Component, OnInit } from '@angular/core';
import { Router, RouterModule } from '@angular/router';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  public dataUsuario:any;

  constructor(public router:Router) {
    this.dataUsuario = JSON.parse(localStorage.getItem("data_go504"));
  }

  ngOnInit(): void {
  }

  volver(){
    this.router.navigateByUrl('/mosaicMenu')
  }

  cambioPass(){
    this.router.navigateByUrl('cambioPassword/2');
  }

}
