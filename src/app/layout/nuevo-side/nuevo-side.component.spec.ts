import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NuevoSideComponent } from './nuevo-side.component';

describe('NuevoSideComponent', () => {
  let component: NuevoSideComponent;
  let fixture: ComponentFixture<NuevoSideComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NuevoSideComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NuevoSideComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
