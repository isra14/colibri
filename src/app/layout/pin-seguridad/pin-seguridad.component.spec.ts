import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PinSeguridadComponent } from './pin-seguridad.component';

describe('PinSeguridadComponent', () => {
  let component: PinSeguridadComponent;
  let fixture: ComponentFixture<PinSeguridadComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PinSeguridadComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PinSeguridadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
