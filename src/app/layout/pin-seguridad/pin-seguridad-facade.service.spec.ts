import { TestBed } from '@angular/core/testing';

import { PinSeguridadFacadeService } from './pin-seguridad-facade.service';

describe('PinSeguridadFacadeService', () => {
  let service: PinSeguridadFacadeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PinSeguridadFacadeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
