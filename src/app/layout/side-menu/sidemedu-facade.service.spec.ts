import { TestBed } from '@angular/core/testing';

import { SidemeduFacadeService } from './sidemedu-facade.service';

describe('SidemeduFacadeService', () => {
  let service: SidemeduFacadeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SidemeduFacadeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
