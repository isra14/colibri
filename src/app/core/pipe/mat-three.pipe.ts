import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'matThree'
})
export class MatThreePipe implements PipeTransform {


  transform(value: any[], arg: any, columna: string[]): any {


    const resultPosts: any = [];
    if (arg == '') {
      return value;
    }

    for (let i = 0; i < columna.length; i++) {
      for (const post of value) {
        for (const child of post.children) {
          if (String(child[columna[i]])?.toLowerCase().indexOf(arg.toLowerCase()) > -1) {
            if (!resultPosts.includes(child)) {
              resultPosts.push(child);
            }
          }
        }

      }
    }

    return resultPosts;
  }

}
