import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SearchPipe } from './search.pipe';
import { SearchFormReactivePipe } from './search-form-reactive.pipe';
import { MatThreePipe } from './mat-three.pipe';
import { MyFilterPipe } from './search-array.pipe';



@NgModule({
  declarations: [SearchPipe, SearchFormReactivePipe, MyFilterPipe],
  imports: [
    CommonModule
  ],
  exports: [
    SearchPipe,
    SearchFormReactivePipe,
    MyFilterPipe
  ]
})
export class PipeModule { }
