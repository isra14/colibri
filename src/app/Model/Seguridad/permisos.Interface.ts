export interface permisosInterface {
    activo: boolean,
    actualizar: boolean,
    eliminar: boolean,
    ingresar: boolean,
    mostrar: boolean,
    visible: boolean,
    vista: boolean,


}

export interface permisosPin {
    idPermisoPin: number,
    descripcion: string,

}

export interface permisosPinUsuario {
    id: number,
    usuario: string,
    idPermisoPin: number,
    descripcion: string,
    fechaRegistro: string
}

export interface usuarioLocationInterface {
    id?: number,
    Usuario?: string,
    idUbicacion?: number,
    ubicacion?: string,
    FechaRegistro?: string
}



export interface locationInterface {
    idUbicacion?: number,
    ubicacion?: string,
    RTN?: string,
    nombrePropietario?: string,
    telefono?: string,
    celular?: string,
    idDepto?: 8,
    idMunicipio?: 110,
    ciudad?: string,
    email?: string,
    direccion?: string,
    idEmpresa?: 0,
    idTipoUbicacion?: 1,
    CAI?: string,
    idCarpeta?: string,
    Horario?: null,
    fecha?: string,
    usuario?: string,
    logo?: string,
    Eslogan?: any,
    Horarios?: any,
    InventarioDiario?: any,
    ImprimeIncierto?: any,
    ImprimeComanda?: any,
    LlenanEncuesta?: any,
    informacionCliente?: any,
    Facbook?: any,
    twitter?: any,
    instagram?: any,
    estatus?: any,
    encargado?: any,
    observaciones?: any,
    diasKardex?: any,
    nombreSucursal?: string
}