export interface menuInterface {
    idMenu?:number;
    descripcion?: string;
    texto?: string;
    icono?: string;
    hijos?:any;
    idPadre?: number;
    orden?: number;
    url?: string;
    activo?: number
    visible?: boolean;
    idModulo?: number;

}

export interface datosPersonalesInterface{
    nombre: string;
}
