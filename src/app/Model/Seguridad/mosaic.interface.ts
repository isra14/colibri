export interface mosaicInterface {
  idModule: number;
  Module: string;
  image: string;
  title: string;
  css: string;
  description: string;
  idStatus: boolean;
 
}