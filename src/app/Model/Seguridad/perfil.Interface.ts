export interface perfilInterface {
    IdPerfil: string,
    idPerfil?: string,
    IdUsuario?:string,
    Nombre?: string,
    Perfil?: string,
    Observaciones: string,
    Activo: boolean
    opcion?: number
}