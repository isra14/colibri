export interface ReportesModulo {
  id: number;
  descripcion: string;
}

export interface ConfiguracionReportes {
  id: number;
  elemento: string;
  titulo: string;
  orden: number;
  propiedad: string;
  obligatorio: boolean;
  grid: string;
  data: any
}


export interface ReporteFinalInterface {

  tables?: ReporteDetalle[];

}

export interface ReporteDetalle{
  tab: string,
  tableTitle: string,
  tableSubTitle: string,
  tableFooter: string,
  th: [],
  data: []
}