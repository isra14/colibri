export interface DataApi {
    data: any;
    sesionActiva: boolean;
    hasError: boolean;
    errors: any;
    token: string;
    newtoken: string;
}