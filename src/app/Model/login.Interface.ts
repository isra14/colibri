export interface loginInterface {
    data: any;
    hasError: boolean;
    errors: any;
    token: string;
    menus: any;
}