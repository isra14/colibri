export interface datosGeneralesInterface {
  nombreEmpresa?: string; 
  direccion?: string; 
  telefono?: string; 
  celular?: string; 
  correoElectronico?: string; 
  rtn?: string; 
  Logo?: string; 
  usuario?: string; 
  fecha?: string; 
  fechaImpresion?: string; 
  horaImpresion?: string; 
}

export interface formasPagoInterface{
  id?: number,
  nombreFormaPago?: string,
  descripcion?: string,
  usuario?: string,
  fecha?: string,
  status ?: boolean,
  idCuentaContable?: number,
}

export interface tarjetasCreditoInterface {
  Id?: number,
  Descripcion?: string,
  Class?: string,
  Color?: string,
}

export interface bancosInterface {
  posId?: number,
  banco?: string,
  pos?: boolean,
}

export interface cuentasBancosInterface {
  idBanco?: number,
  cuentaBancaria?: string,
}