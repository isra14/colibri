import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { paginatorInterface } from 'src/app/Model/Shared/paginator.interface';

@Injectable({
  providedIn: 'root'
})
export class PaginatorFacadeService {
  private DataPaginator = new BehaviorSubject<paginatorInterface>({ desde: 0, hasta: 0, itemPagina: 0, pageIndex: 0, totalItem: 0 });
  public responseDataPaginator: Observable<paginatorInterface> = this.DataPaginator.asObservable();

  constructor() { }

  cambiarValor() {
    
  }
}
