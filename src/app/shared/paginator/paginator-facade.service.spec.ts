import { TestBed } from '@angular/core/testing';

import { PaginatorFacadeService } from './paginator-facade.service';

describe('PaginatorFacadeService', () => {
  let service: PaginatorFacadeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PaginatorFacadeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
