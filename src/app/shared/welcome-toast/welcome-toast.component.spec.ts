import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WelcomeToastComponent } from './welcome-toast.component';

describe('WelcomeToastComponent', () => {
  let component: WelcomeToastComponent;
  let fixture: ComponentFixture<WelcomeToastComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WelcomeToastComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WelcomeToastComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
