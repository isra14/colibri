import { TestBed } from '@angular/core/testing';

import { MensajesHttpService } from './mensajes-http.service';

describe('MensajesHttpService', () => {
  let service: MensajesHttpService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MensajesHttpService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
