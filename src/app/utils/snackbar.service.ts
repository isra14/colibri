import { Injectable } from '@angular/core';
import {MatSnackBar} from '@angular/material/snack-bar';

@Injectable({
  providedIn: 'root'
})
export class SnackbarService {

  constructor(private _snackBar: MatSnackBar) { }

  snackbarSimple(mensaje: string, accion: string) {
    this._snackBar.open(mensaje, accion, {
      duration: 2000,
    });
  }

  snackbarSimpleColor(mensaje: string, accion: string,color:string) {
    this._snackBar.open(mensaje, accion, {
      duration: 2000,
      panelClass:[color],
      // verticalPosition:'bottom',
      // horizontalPosition:'center',

    });
  }
}
